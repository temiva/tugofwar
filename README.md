# TugOfWar

Source code for Tug of War Android game.

**Compiled build can be found here**

https://drive.google.com/file/d/1WyzVSvcFP7Y9qh1kYWO-PNcxL4PEI_8R/view?usp=sharing

Google drive link above is recommended since Drive allows APK installs directly!

Alternatively you can also download APK directly from gitlab:

https://gitlab.com/temiva/tugofwar/tree/master/APK

Please note! Atleast Android 9 with chrome has disabled installing downloaded.APK
files. You have to use alternative browser (e.g. Firefox or Dolphin) to download the game.

![alt text](Screenshot.png)