LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES :=  $(LOCAL_PATH)/hellocpp/main.cpp \
                    $(LOCAL_PATH)/../../../Classes/AppDelegate.cpp \
                    $(LOCAL_PATH)/../../../Classes/RaceScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/RaceUILayer.cpp \
                    $(LOCAL_PATH)/../../../Classes/Vehicle.cpp \
                    $(LOCAL_PATH)/../../../Classes/VehicleInput.cpp \
                    $(LOCAL_PATH)/../../../Classes/Wheel.cpp \
                    $(LOCAL_PATH)/../../../Classes/AIController.cpp \
                    $(LOCAL_PATH)/../../../Classes/VehicleBuildHelper.cpp \
                    $(LOCAL_PATH)/../../../Classes/Race.cpp \
                    $(LOCAL_PATH)/../../../Classes/SceneLoader.cpp \
                    $(LOCAL_PATH)/../../../Classes/GarageScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/Gauge.cpp \
                    $(LOCAL_PATH)/../../../Classes/DistanceMeter.cpp \
                    $(LOCAL_PATH)/../../../Classes/ExhaustPipe.cpp \
                    $(LOCAL_PATH)/../../../Classes/VehicleConfig.cpp \
                    $(LOCAL_PATH)/../../../Classes/Session.cpp \
                    $(LOCAL_PATH)/../../../Classes/OpponentSelectScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/TweenButton.cpp \
                    $(LOCAL_PATH)/../../../Classes/TweenGroup.cpp \
                    $(LOCAL_PATH)/../../../Classes/UpgradeFrame.cpp \
                    $(LOCAL_PATH)/../../../Classes/Engine.cpp




LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cc_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module, cocos)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
