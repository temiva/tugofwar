//
// Created by temiv on 4/9/2019.
//

#ifndef PROJ_ANDROID_TEMPERATUREGAUGE_H
#define PROJ_ANDROID_TEMPERATUREGAUGE_H

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

/**
 * @brief Analog gauge UI element.
 */
class Gauge : public Node
{
private:
    Sprite *m_pin;
    Sprite *m_warningBackground;
    float m_val;
    float m_flashTimer;

public:
    CREATE_FUNC(Gauge);

    bool init() override;

    void setValue(float val);

    void update(float delta) override;
};


#endif //PROJ_ANDROID_TEMPERATUREGAUGE_H
