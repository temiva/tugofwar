//
// Created by temiv on 4/12/2019.
//

#include "TweenButton.h"


TweenButton *
TweenButton::create(const std::string &normalImage, const std::string &selectedImage, const std::string &disableImage, float delay,
    bool runNow)
{
    TweenButton *btn = new(std::nothrow) TweenButton;
    if (btn && btn->init(normalImage, selectedImage, disableImage, delay, runNow))
    {
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

bool TweenButton::init(const std::string &normalImage, const std::string &selectedImage, const std::string &disableImage, float delay,
    bool runNow)
{
    if (!Button::init(normalImage, selectedImage, disableImage))
        return false;

    m_delay = delay;
    if (runNow)
        animateIn();

    return true;
}

void TweenButton::animateIn()
{
    this->setScale(0.0f);
    auto btnScaleTo = ScaleTo::create(m_duration, 1.0f);
    auto delay = DelayTime::create(m_delay);
    auto scaleEased = EaseBackOut::create(btnScaleTo->clone());

    auto sequence = Sequence::create(delay, scaleEased, nullptr);
    this->runAction(RepeatForever::create(sequence));
}

void TweenButton::setVisible(bool visible)
{
    if (isVisible() != visible)
    {
        Button::setVisible(visible);
        if (visible == true)
        {
            animateIn();
        }
    }
}