//
// Created by temiv on 4/3/2019.
//

#include "VehicleInput.h"
#include "Vehicle.h"

USING_NS_CC;

const string VehicleInput::EVT_USER_INPUT_SET = "EVT_USER_INPUT_SET";
const string VehicleInput::EVT_AI_INPUT_SET = "EVT_AI_INPUT_SET";

void VehicleInput::inputHandler(EventCustom *event)
{
    VehicleInputValue *carInput = static_cast<VehicleInputValue *>(event->getUserData());
    m_vehicle->setGasLevel(carInput->gasLevel);
    m_vehicle->setBrake(carInput->brake);
}

void VehicleInput::listenPlayerInput()
{
    EventListenerCustom *gasListener = EventListenerCustom::create(EVT_USER_INPUT_SET, CC_CALLBACK_1(VehicleInput::inputHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(gasListener, this);
}

void VehicleInput::listenAIInput()
{
    EventListenerCustom *gasListener = EventListenerCustom::create(EVT_AI_INPUT_SET, CC_CALLBACK_1(VehicleInput::inputHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(gasListener, this);
}