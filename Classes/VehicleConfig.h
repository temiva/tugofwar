//
// Created by temiv on 4/10/2019.
//

#ifndef PROJ_ANDROID_VEHICLECONFIG_H
#define PROJ_ANDROID_VEHICLECONFIG_H

#include "cocos2d.h"
#include <string>
#include <vector>

using namespace cocos2d;
using namespace std;

/**
 * @brief Defines how a vehicle part should be assembled into a vehicle. Each VehiclePartConfig can contain many
 * visual sprites but share the same configuration.
 */
struct VehiclePartConfig
{
private:
    vector<string> m_files;
    int m_selectedSprite = 0;

public:
    /**
     * @param spriteFiles sprite variations. For example different sprite colors in a vector
     * @param position sprite position in local vehicle space
     * @param bodySize size of PhysicsBody component rectangle
     * @param bodyOffset offset for the PhysicsBody. Sprite will be offset from collider rectangle
     * @param isDetachable is this cosmetic part that can be detached during gameplay
     * @param physicsMaterial physics body's material.
     */
    VehiclePartConfig(vector<string> spriteFiles, Vec2 position, Size bodySize, Vec2 bodyOffset, bool isDetachable,
        PhysicsMaterial physicsMaterial) : m_files(spriteFiles), position(position), bodySize(bodySize), bodyOffset(bodyOffset),
                                           isDetachable(isDetachable), physicsMaterial(physicsMaterial)
    {};

    Vec2 position;
    Size bodySize;
    Vec2 bodyOffset;
    PhysicsMaterial physicsMaterial;
    bool isDetachable;

    void selectSpriteVariation(int index)
    {
        m_selectedSprite = index;
        if (m_selectedSprite > m_files.size() - 1)
            m_selectedSprite = m_files.size() - 1;
        if (m_selectedSprite < 0)
            m_selectedSprite = 0;
    }

    string getSpriteFilename()
    { return m_files[m_selectedSprite]; };
};

struct WheelConfig
{
    /**
     * @param position in local vehicle space
     * @param isDriveWheel defines if Wheel's torque sets work.
     */
    WheelConfig(Vec2 position, bool isDriveWheel) : position(position), isDriveWheel(isDriveWheel)
    {};

    Vec2 position;
    bool isDriveWheel;
};

struct EngineConfig
{
    /**
     * @param position in local vehicle space
     * @param bodySize Physical collider rectangle size
     */
    EngineConfig(Vec2 position, Size bodySize) : position(position), bodySize(bodySize)
    {};

    Vec2 position;
    Size bodySize;
};


/**
 * @brief struct for vehicle's engine level. Defines engines's features when vehicle is created
 */
struct EngineLevel
{
    EngineLevel(string spriteFilePath, int cost, float torque, float overheatSpeed, Vec2 positionOffset, PhysicsMaterial physicsMaterial)
        : spriteFilePath(spriteFilePath), cost(cost), torque(torque), overheatSpeed(overheatSpeed), positionOffset(positionOffset),
          physicsMaterial(physicsMaterial)
    {};

    string spriteFilePath;
    int cost;
    float torque;
    float overheatSpeed;
    Vec2 positionOffset;
    PhysicsMaterial physicsMaterial;
};

/**
 * @brief struct for vehicle's wheel level. Defines wheels's features when vehicle is created
 */
struct WheelLevel
{
    WheelLevel(string spriteFilePath, int cost, float radius, float braking, PhysicsMaterial physicsMaterial) : spriteFilePath(
        spriteFilePath), cost(cost), radius(radius), braking(braking), physicsMaterial(physicsMaterial)
    {};

    string spriteFilePath;
    int cost;
    float radius;
    float braking;
    PhysicsMaterial physicsMaterial;
};

/**
 * @brief struct for vehicle's suspension level. Defines suspension's features when vehicle is created
 */
struct SuspensionLevel
{
    SuspensionLevel(string spriteFilePath, int cost, float springLength, float springPower, float springDamping) : spriteFilePath(
        spriteFilePath), cost(cost), springLength(springLength), springPower(springPower), springDamping(springDamping)
    {};

    string spriteFilePath;
    int cost;
    float springLength;
    float springPower;
    float springDamping;
};


namespace VehicleConfig
{
    //Available vehicle parts
    extern const EngineConfig truckEngine;
    extern const vector<WheelConfig> truckWheels;
    extern const VehiclePartConfig truckCenter;
    extern const vector<VehiclePartConfig> truckBackFrames;
    extern const vector<VehiclePartConfig> truckFrontFrames;
    extern const vector<VehiclePartConfig> truckBuckets;
    extern const vector<VehiclePartConfig> truckCabinFronts;
    extern const vector<VehiclePartConfig> truckCabins;
    extern const vector<VehiclePartConfig> truckSteppers;
    extern const vector<VehiclePartConfig> truckBackFenders;
    extern const vector<VehiclePartConfig> truckHoods;
    extern const vector<VehiclePartConfig> truckFrontFenders;
    extern const vector<VehiclePartConfig> truckDoors;

    /**
     * @brief All available engine levels in a vector. First element is lvl0, second lvl1... and so on
     */
    extern const vector<EngineLevel> engineLevels;
    /**
     * @brief All available wheel levels in a vector. First element is lvl0, second lvl1... and so on
     */
    extern const vector<WheelLevel> wheelLevels;
    /**
     * @brief All available suspension levels in a vector. First element is lvl0, second lvl1... and so on
     */
    extern const vector<SuspensionLevel> suspensionLevels;
};


#endif //PROJ_ANDROID_VEHICLECONFIG_H
