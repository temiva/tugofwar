//
// Created by temiv on 4/10/2019.
//

#include "Session.h"

const vector<VehiclePartConfig> Session::vehicleCosmetics = {VehicleConfig::truckBackFrames[0], VehicleConfig::truckFrontFrames[0],
                                                             VehicleConfig::truckBuckets[0], VehicleConfig::truckCabinFronts[0],
                                                             VehicleConfig::truckCabins[0], VehicleConfig::truckSteppers[0],
                                                             VehicleConfig::truckBackFenders[0], VehicleConfig::truckHoods[0],
                                                             VehicleConfig::truckFrontFenders[0], VehicleConfig::truckDoors[0]};

Session::Session(int money, int wheelLevel, int engineLevel, int suspensionLevel) : m_money(money), m_wheelLevel(wheelLevel),
                                                                                    m_engineLevel(engineLevel),
                                                                                    m_suspensionLevel(suspensionLevel)
{}

bool Session::canUpgradeWheel()
{
    return m_wheelLevel < VehicleConfig::wheelLevels.size() - 1 && m_money > VehicleConfig::wheelLevels[m_wheelLevel + 1].cost;
}

bool Session::canUpgradeEngine()
{
    return m_engineLevel < VehicleConfig::engineLevels.size() - 1 && m_money > VehicleConfig::engineLevels[m_engineLevel + 1].cost;
}

bool Session::canUpgradeSuspension()
{
    return m_suspensionLevel < VehicleConfig::suspensionLevels.size() - 1 &&
           m_money > VehicleConfig::suspensionLevels[m_suspensionLevel + 1].cost;
}

void Session::upgradeWheel()
{
    if (canUpgradeWheel())
    {
        m_money -= 300;
        m_wheelLevel++;
    }
}

void Session::upgradeEngine()
{
    if (canUpgradeEngine())
    {
        m_money -= 300;
        m_engineLevel++;
    }
}

void Session::upgradeSuspension()
{
    if (canUpgradeSuspension())
    {
        m_money -= 300;
        m_suspensionLevel++;
    }
}

void Session::downgradeWheel()
{
    if (canDowngradeWheel())
    {
        m_money += 300;
        m_wheelLevel--;
    }
}

void Session::downgradeEngine()
{
    if (canDowngradeEngine())
    {
        m_money += 300;
        m_engineLevel--;
    }
}

void Session::downgradeSuspension()
{
    if (canDowngradeSuspension())
    {
        m_money += 300;
        m_suspensionLevel--;
    }
}