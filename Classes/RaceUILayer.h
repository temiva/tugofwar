//
// Created by temiv on 4/3/2019.
//

#ifndef PROJ_ANDROID_TOUCHLAYER_H
#define PROJ_ANDROID_TOUCHLAYER_H

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "SceneLoader.h"
#include "Gauge.h"
#include "DistanceMeter.h"
#include "TweenButton.h"

using namespace cocos2d;
using namespace cocos2d::ui;

/**
 * @brief Race ui containing all visual elements and touch input handlers.
 */
class RaceUILayer : public Layer
{
private:
    SceneLoader *m_gameState;
    Label *m_labelTouchInfo;
    Label *m_timeLabel;
    TweenButton *m_restartButton;
    TweenButton *m_garageButton;
    Gauge *m_gauge;
    DistanceMeter *m_distanceMeter;
    Sprite *m_loseSprite;
    Sprite *m_vehicleStatus;
    Sprite *m_winSprite;
    Sprite *m_gasInputSprite;
    Sprite *m_brakeInputSprite;

    Vec2 m_brakeDefaultPosition;
    float m_gasInput = 0.0f;
    bool m_brakeInput = false;
    int m_raceTimeElapsed = 0;
    float m_pedalPaddingTop = 0.55f;
    float m_pedalPaddingBottom = 0.4f;

    void setupEventListeners();

    void setupUIElements();

    float getPedalMinHeight();

    float getPedalMaxHeight();

    void updateTouchInput(Touch *touch, bool touchRelease);

    void resetGasInput();

    void setGasPedalPosition(float gasLevel);

    //Event handlers
    void playerVehicleUpdatedHandler(EventCustom *event);

    void countdownSetHandler(EventCustom *event);

    void raceStartedHandler(EventCustom *event);

    void raceTimeSetHandler(EventCustom *event);

    void raceWonHandler(EventCustom *event);

    void raceLostHandler(EventCustom *event);

    void raceEndedHandler(EventCustom *event);

    //Button events
    void onRestartButton(Ref *pSender, Widget::TouchEventType type);

    void onGarageButton(Ref *pSender, Widget::TouchEventType type);

public:
    static RaceUILayer *create(SceneLoader *gameState);

    bool init(SceneLoader *gameState);

    bool onTouchBegan(Touch *touch, Event *event) override;

    void onTouchEnded(Touch *touch, Event *event) override;

    void onTouchMoved(Touch *touch, Event *event) override;

    void onTouchCancelled(Touch *touch, Event *event) override;

};


#endif //PROJ_ANDROID_TOUCHLAYER_H
