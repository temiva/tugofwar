//
// Created by temiv on 4/5/2019.
//

#ifndef PROJ_ANDROID_VEHICLEBUILDHELPER_H
#define PROJ_ANDROID_VEHICLEBUILDHELPER_H

#include "cocos2d.h"
#include <string>

using namespace cocos2d;
using namespace std;

namespace VehicleBuildHelper
{
    /**
     * @brief Creates a sprite and assigns given physics body to it
     */
    Sprite *createSprite(Vec2 position, string filename, PhysicsBody *body);

    /**
     * @brief Creates a circle physics body to given scene
     */
    Sprite *createCircleBody(Scene &scene, float radius, Vec2 position, Vec2 bodyOffset, string filename, PhysicsMaterial physicsMaterial,
        int categoryBitmask, int collisionBitmask);

    /**
     * @brief Creates a rectangle physics body to given scene
     */
    Sprite *createRectangleBody(Scene &scene, Size size, Vec2 position, Vec2 bodyOffset, string filename, PhysicsMaterial physicsMaterial,
        int categoryBitmask, int collisionBitmask);

    /**
     * @brief Joins two physics bodies together using a fixed join. Alternative to Coco's default fixed joint
     * @param anchor in world space
     */
    void joinFixed(Scene &scene, PhysicsBody *bodyA, PhysicsBody *bodyB, Vec2 anchor);
}
#endif //PROJ_ANDROID_VEHICLEBUILDHELPER_H
