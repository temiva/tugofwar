//
// Created by temiv on 4/9/2019.
//

#ifndef PROJ_ANDROID_CARMENUSCENE_H
#define PROJ_ANDROID_CARMENUSCENE_H

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "SceneLoader.h"
#include "Session.h"
#include "TweenButton.h"
#include "TweenGroup.h"
#include "UpgradeFrame.h"
#include <string>
#include <memory>

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace std;

/**
 * @brief Garage UI scene description.
 */
class GarageScene : public Scene
{
private:
    SceneLoader *m_gameState;
    Session *m_sessionData;
    unique_ptr<TweenGroup> m_animationGroup;
    TweenButton *m_nextButton;
    TweenButton *m_cheatButton;
    Label *m_money;

    UpgradeFrame *m_engineUpgradeFrame;
    UpgradeFrame *m_wheelUpgradeFrame;
    UpgradeFrame *m_suspensionUpgradeFrame;

    void setupUI();

    void updateUI();

    void loadLevelMenu(float delta);

public:
    static GarageScene *create(SceneLoader *gameState, Session *sessionData);

    bool init(SceneLoader *gameState, Session *sessionData);
};


#endif //PROJ_ANDROID_CARMENUSCENE_H
