//
// Created by temiv on 4/11/2019.
//

#ifndef PROJ_ANDROID_LEVELMENUSCENE_H
#define PROJ_ANDROID_LEVELMENUSCENE_H

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "SceneLoader.h"
#include "Session.h"
#include "Vehicle.h"
#include "TweenButton.h"
#include "TweenGroup.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class OpponentSelectScene : public Scene
{
private:
    SceneLoader *m_gameState;
    Session *m_sessionData;
    unique_ptr<TweenGroup> m_animationGroup;

    int m_enemyLevel = 0;
    TweenButton *m_raceButton;
    TweenButton *m_backButton;
    TweenButton *m_nextLevelButton;
    TweenButton *m_prevLevelButton;
    Label *m_headerLabel;
    Label *m_rewardLabel;
    Vehicle *m_enemyPreview;

    void randomizeEnemy(int level);

public:
    static OpponentSelectScene *create(SceneLoader *gameState, Session *sessionData);

    bool init(SceneLoader *gameState, Session *sessionData);

    void SetupUI();

    void UpdateUI();
};

#endif //PROJ_ANDROID_LEVELMENUSCENE_H
