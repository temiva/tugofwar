//
// Created by temiv on 4/2/2019.
//

#ifndef PROJ_ANDROID_GAMESCENE_H
#define PROJ_ANDROID_GAMESCENE_H

#include "cocos2d.h"
#include "RaceUILayer.h"
#include "Vehicle.h"
#include "AIController.h"
#include "Race.h"
#include "SceneLoader.h"

using namespace cocos2d;
using namespace std;

/**
 * @brief Race scene containing gameplay elements, cameras and vehicles
 */
class RaceScene : public Scene
{
private:
    SceneLoader *m_gameState;
    Session *m_sessionData;
    Vehicle *m_vehiclePlayer;
    Vehicle *m_vehicleAI;
    Sprite *m_ropeSprite;
    AIController *m_aiController;
    Camera *m_worldCamera;
    Camera *m_camHUD;
    Race *m_race;

    void raceWonHandler(EventCustom *event);

    void setupCameras();

    void setupSceneSprites();

    void setupVehicles();

public:
    static RaceScene *create(SceneLoader *gameState, Session *sessionData);

    bool init(SceneLoader *gameState, Session *sessionData);

    void createGround();

    void ConnectVehicles(PhysicsBody *bodyA, PhysicsBody *bodyB, Vec2 localAttachPointA, Vec2 localAttachPointB, float connectionDistance);

    void update(float delta) override;
};


#endif //PROJ_ANDROID_GAMESCENE_H
