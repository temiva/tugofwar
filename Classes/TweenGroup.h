//
// Created by temiv on 4/12/2019.
//

#ifndef PROJ_ANDROID_ANIMATIONGROUP_H
#define PROJ_ANDROID_ANIMATIONGROUP_H


#include "cocos2d.h"
#include "ui\CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace std;


/**
 * @brief Takes a group of Node's and animates them into view and out of view
 */
class TweenGroup
{
private:
    vector<Node *> m_nodes;
    float m_delayBetweenElements;
    float m_duration = 0.35f;

public:
    TweenGroup(float delayBetweenElements) { m_delayBetweenElements = delayBetweenElements; };

    void addNode(Node *node) { m_nodes.push_back(node); };

    void AnimateSlideOut(int dir);

    void AnimateSlideIn(int dir);
};


#endif //PROJ_ANDROID_ANIMATIONGROUP_H
