//
// Created by temiv on 4/10/2019.
//

#include "ExhaustPipe.h"

ExhaustPipe *ExhaustPipe::create(string textureFile, string smokeParticleFile, string flameParticleFile)
{
    ExhaustPipe *exhaust = new(std::nothrow) ExhaustPipe;
    if (exhaust && exhaust->init(textureFile, smokeParticleFile, flameParticleFile))
    {
        exhaust->autorelease();
        return exhaust;
    }
    delete exhaust;
    exhaust = nullptr;
    return nullptr;
}

bool ExhaustPipe::init(string textureFile, string smokeParticleFile, string flameParticleFile)
{
    if (!Node::init())
        return false;

    m_sprite = Sprite::create(textureFile);
    this->addChild(m_sprite);
    this->setSmokeParticle(ParticleSystemQuad::create(smokeParticleFile));
    this->setFlameParticle(ParticleSystemQuad::create(flameParticleFile));

    this->scheduleUpdate();

    return true;
}


void ExhaustPipe::setSmokeParticle(ParticleSystemQuad *particleSystem)
{
    m_smokeParticle = particleSystem;

    if (m_smokeParticle)
    {
        m_smokeParticle->setScale(0.25f);
        m_smokeParticle->setPosition(Vec2(2, 19));
        m_smokeParticle->setPositionType(ParticleSystem::PositionType::FREE);

        m_defaultSmokeEmissionRate = m_smokeParticle->getEmissionRate();
        m_defaultSmokeEmissionSpeed = m_smokeParticle->getSpeed();
        m_defaultSmokeStartColor = m_smokeParticle->getStartColor();
        m_defaultSmokeEndColor = m_smokeParticle->getEndColor();
        m_defaultSmokeStartSize = m_smokeParticle->getStartSize();
        m_defaultSmokeEndSize = m_smokeParticle->getEndSize();

        addChild(m_smokeParticle, 100);
    }
    setGasValue(0);
}

void ExhaustPipe::setFlameParticle(ParticleSystemQuad *particleSystem)
{
    m_flameParticle = particleSystem;
    if (m_flameParticle)
    {
        m_flameParticle->setScale(0.25f);
        m_flameParticle->setPosition(Vec2(3, 17));
        m_flameParticle->setPositionType(ParticleSystem::PositionType::FREE);

        m_defaultFlameEmissionRate = m_flameParticle->getEmissionRate();
        m_defaultFlameEmissionSpeed = m_flameParticle->getSpeed();
        m_defaultFlameStartSize = m_flameParticle->getStartSize();
        m_defaultFlameLife = m_flameParticle->getLife();

        m_flameParticle->setEmissionRate(0);
        addChild(m_flameParticle, 101);
    }
}

void ExhaustPipe::setGasValue(float val)
{
    if (m_smokeParticle)
    {
        //Increase emit rate, amount and size based on gas value
        m_smokeParticle->setEmissionRate(MathUtil::lerp(m_defaultSmokeEmissionRate * 0.6f, m_defaultSmokeEmissionRate, val));
        m_smokeParticle->setSpeed(MathUtil::lerp(m_defaultSmokeEmissionSpeed * 0.2f, m_defaultSmokeEmissionSpeed, val));
        m_smokeParticle->setStartSize(MathUtil::lerp(m_defaultSmokeStartSize * 0.6f, m_defaultSmokeStartSize, val));
        m_smokeParticle->setEndSize(MathUtil::lerp(m_defaultSmokeEndSize * 0.4f, m_defaultSmokeEndSize, val));

        //Start color alpha set
        Color4F startColor = m_defaultSmokeStartColor;
        startColor.a = MathUtil::lerp(0.2f * m_defaultSmokeStartColor.a, 1.0f * m_defaultSmokeStartColor.a, val);
        m_smokeParticle->setStartColor(startColor);

        //End color alpha set
        Color4F endColor = m_defaultSmokeEndColor;
        endColor.a = MathUtil::lerp(0.2f * m_defaultSmokeEndColor.a, 1.0f * m_defaultSmokeEndColor.a, val);
        m_smokeParticle->setEndColor(endColor);
    }
}

void ExhaustPipe::setTemperature(float val)
{
    if (m_flameParticle)
    {
        float flameThresshold = 0.65f;
        float t = (val - flameThresshold) / flameThresshold;


        //Flames pop somewhat randomly. More likely to happen when t is high
        bool showFlame = (t + 0.18f) > ((rand() % 1000) * 0.001f);

        if (showFlame && val > flameThresshold)
        {
            //Increase emit rate and amount based on temperature
            m_flameParticle->setEmissionRate(MathUtil::lerp(m_defaultFlameEmissionRate * 0.5f, m_defaultFlameEmissionRate, t));
            m_flameParticle->setSpeed(MathUtil::lerp(m_defaultFlameEmissionSpeed * 0.5f, m_defaultFlameEmissionSpeed, t));
            m_flameParticle->setStartSize(MathUtil::lerp(m_defaultFlameStartSize * 0.6f, m_defaultFlameStartSize, t));
            m_flameParticle->setLife(MathUtil::lerp(m_defaultFlameLife * 0.4f, m_defaultFlameLife, t));
        }
        else
        {
            //No flames at low temps
            m_flameParticle->setEmissionRate(0);
        }
    }
}
