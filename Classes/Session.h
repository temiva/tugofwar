//
// Created by temiv on 4/10/2019.
//

#ifndef PROJ_ANDROID_SESSIONDATA_H
#define PROJ_ANDROID_SESSIONDATA_H

#include "VehicleConfig.h"
#include "Vehicle.h"

/**
 * @brief Contains current gameplay session's player data. What level player has upgraded each vehicle part
 * money value and current chosen opponent vehicle setup.
 */
class Session
{

private:
    int m_wheelLevel = 1;
    int m_engineLevel = 1;
    int m_suspensionLevel = 1;
    int m_money = 500;
    VehicleSetup m_enemyVehicleSetup;
public:
    static const vector<VehiclePartConfig> vehicleCosmetics;

    Session(int money, int wheelLevel, int engineLevel, int suspensionLevel);

    bool canUpgradeWheel();

    bool canUpgradeEngine();

    bool canUpgradeSuspension();

    bool canDowngradeWheel() { return m_wheelLevel > 0; };

    bool canDowngradeEngine() { return m_engineLevel > 0; };

    bool canDowngradeSuspension() { return m_suspensionLevel > 0; };

    void upgradeWheel();

    void upgradeEngine();

    void upgradeSuspension();

    void downgradeWheel();

    void downgradeEngine();

    void downgradeSuspension();

    int getWheelLevel() { return m_wheelLevel; };

    int getEngineLevel() { return m_engineLevel; };

    int getSuspensionLevel() { return m_suspensionLevel; };

    int getMoney() { return m_money; };

    void addMoney(int amount) { m_money += amount; };

    void setEnemyVehicleSetup(VehicleSetup vehicle) { m_enemyVehicleSetup = vehicle; };

    VehicleSetup *getEnemyVehicleSetup() { return &m_enemyVehicleSetup; };
};


#endif //PROJ_ANDROID_SESSIONDATA_H
