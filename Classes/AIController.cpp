//
// Created by temiv on 4/5/2019.
//

#include "AIController.h"

USING_NS_CC;


AIController *AIController::create(Vehicle *vehicle)
{
    AIController *aiController = new(std::nothrow) AIController;
    if (aiController && aiController->init(vehicle))
    {
        aiController->autorelease();
        return aiController;
    }
    delete aiController;
    aiController = nullptr;
    return nullptr;
}

bool AIController::init(Vehicle *vehicle)
{
    if (!Node::init())
        return false;

    m_vehicle = vehicle;
    this->scheduleUpdate();

    return true;
}

void AIController::SetIdleState()
{
    state = AIState::Idle;
    m_idleTimeTarget = 0.6f + 1.5f * ((rand() % 1000) * 0.001f);
}

void AIController::SetAccelerateState()
{
    state = AIState::Accelerate;
    m_targetGasLevel = 0.3f + 0.7f * ((rand() % 1000) * 0.001f);
    m_nextTempTarget = 0.6f + 0.4f * ((rand() % 1000) * 0.001f);
}

void AIController::SetBrakeState()
{
    state = AIState::Brake;
    m_nextTempTarget = 0.2f + 0.7f * ((rand() % 1000) * 0.001f);
}

void AIController::UpdateState(float dt)
{
    m_idleTime += dt;
    if (m_targetGasLevel == 0 || (state == AIState::Accelerate && m_vehicle->getEngine()->getTemperature() > m_nextTempTarget))
    {
        //Randomly select idle or brake state
        if (rand() % 2 == 1)
            SetIdleState();
        else
            SetBrakeState();
    }
    //Braked long enough to reach target temp state -> start accelerating again
    if (state == AIState::Brake && m_vehicle->getEngine()->getTemperature() < m_nextTempTarget)
        SetAccelerateState();

    //Idled target idle time -> start accelerating again
    if (state == AIState::Idle && m_idleTime >= m_idleTimeTarget)
        SetAccelerateState();
}

VehicleInputValue AIController::getInput()
{
    float gasInput = 0.0f;//1.0;
    bool brakeInput = false;

    if (state == AIState::Accelerate)
    {
        gasInput = m_targetGasLevel;
        brakeInput = false;
    }
    else if (state == AIState::Brake)
    {
        gasInput = 0.0f;
        brakeInput = true;
    }
    else if (state == AIState::Idle)
    {
        gasInput = 0.0f;
        brakeInput = false;
    }
    gasInput = min(1.0f, max(gasInput, 0.0f)); //clamp to 0-1

    return VehicleInputValue{gasInput, brakeInput};
}

void AIController::update(float dt)
{
    UpdateState(dt);


    VehicleInputValue input = getInput();
    EventCustom event(VehicleInput::EVT_AI_INPUT_SET);
    event.setUserData(&input);
    _eventDispatcher->dispatchEvent(&event);
}


