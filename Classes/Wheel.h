//
// Created by temiv on 4/3/2019.
//

#ifndef PROJ_ANDROID_WHEEL_H
#define PROJ_ANDROID_WHEEL_H

#include "cocos2d.h"
#include <string>

using namespace cocos2d;
using namespace std;

class Wheel : public Node
{
private:
    PhysicsBody *m_body;
    Sprite *m_sprite;
    float m_torque = 0;
    float m_brakePower = 0;
    bool m_isDriveWheel;

public:
    /**
    @param scene scene where the wheel will be added to
    @param filename texture filename / path
    @param position in world space
    @param isDriveWheel can this wheel apply torque?
    @param categoryBitmask physics object's bitmask (collision layer)
    @param collisionBitmask physics object's bitmask that this wheel will collide with
    */
    static Wheel *
    create(Scene &scene, string filename, float radius, float brakePower, Vec2 position, bool isDriveWheel, PhysicsMaterial physicsMaterial,
        int categoryBitmask, int collisionBitmask);

    bool
    init(Scene &scene, string filename, float radius, float brakePower, Vec2 position, bool isDriveWheel, PhysicsMaterial physicsMaterial,
        int categoryBitmask, int collisionBitmask);

    void update(float dt) override;

    void setTorque(float torque);

    void setBrake(bool brake);

    PhysicsBody *getBody() { return m_body; };
};


#endif //PROJ_ANDROID_WHEEL_H
