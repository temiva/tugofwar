//
// Created by temiv on 4/8/2019.
//

#ifndef PROJ_ANDROID_RACE_H
#define PROJ_ANDROID_RACE_H

#include "cocos2d.h"
#include "Vehicle.h"

using namespace cocos2d;
using namespace std;

/**
 * @brief Race status class. Defines if race lifetime status and informs race events to others using global events
 */
class Race : public Node
{
private:
    Vehicle *m_player;
    Vehicle *m_enemy;
    float m_raceTimer = 0;
    bool m_raceStarted;

    void end();

public:
    static const float DISTANCE_THRESSHOLD;
    static const string EVT_RACE_COUNTDOWN_SET;
    static const string EVT_RACE_STARTED;
    static const string EVT_RACE_TIME_SET;
    static const string EVT_RACE_ENDED;
    static const string EVT_RACE_WON;
    static const string EVT_RACE_LOST;

    void startCountdown(Vehicle *player, Vehicle *enemy);

    void update(float deltaTime) override;

    CREATE_FUNC(Race);
};


#endif //PROJ_ANDROID_RACE_H
