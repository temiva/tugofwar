//
// Created by temiv on 4/5/2019.
//

#include "VehicleBuildHelper.h"

Sprite *VehicleBuildHelper::createSprite(Vec2 position, string filename, PhysicsBody *body)
{
    auto sprite = Sprite::create(filename);
    if (body)
        sprite->setPhysicsBody(body);
    sprite->setPosition(position);
    return sprite;
}

Sprite *VehicleBuildHelper::createCircleBody(Scene &scene, float radius, Vec2 position, Vec2 bodyOffset, string filename,
    PhysicsMaterial physicsMaterial, int categoryBitmask, int collisionBitmask)
{
    auto body = PhysicsBody::createCircle(radius, physicsMaterial);
    body->setPositionOffset(bodyOffset);
    body->setDynamic(true);
    body->setCollisionBitmask(collisionBitmask);
    body->setCategoryBitmask(categoryBitmask);

    return createSprite(position, filename, body);
}

Sprite *VehicleBuildHelper::createRectangleBody(Scene &scene, Size size, Vec2 position, Vec2 bodyOffset, string filename,
    PhysicsMaterial physicsMaterial, int categoryBitmask, int collisionBitmask)
{
    auto body = PhysicsBody::createBox(size, physicsMaterial);
    body->setPositionOffset(bodyOffset);
    body->setDynamic(true);
    body->setCollisionBitmask(collisionBitmask);
    body->setCategoryBitmask(categoryBitmask);

    return createSprite(position, filename, body);
}


//Fixed join is just pin joint + gear joint together. This does the same without FixedJoint's
//weird anchor behaviour. Anchor is in world space. Bodies should be placed in their desired positions before anchoring them.
//For some reason Cocos2D-x PhysicsJoinFixed moves both connected bodies to its anchor point. Its the only
//joint type that does this.
//https://github.com/cocos2d/cocos2d-x/blob/v3/cocos/physics/CCPhysicsJoint.cpp
//It doesn't make any sense and you'd have to deal with collider offsets etc. complicated.
void VehicleBuildHelper::joinFixed(Scene &scene, PhysicsBody *bodyA, PhysicsBody *bodyB, Vec2 anchor)
{
    PhysicsJointPin *pinJoint = PhysicsJointPin::construct(bodyA, bodyB, anchor);
    PhysicsJointGear *gearJoint = PhysicsJointGear::construct(bodyA, bodyB, 0, 1);
    scene.getPhysicsWorld()->addJoint(pinJoint);
    scene.getPhysicsWorld()->addJoint(gearJoint);
}