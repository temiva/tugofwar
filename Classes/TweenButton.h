//
// Created by temiv on 4/12/2019.
//

#ifndef PROJ_ANDROID_TWEENBUTTON_H
#define PROJ_ANDROID_TWEENBUTTON_H


#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include <string>

using namespace cocos2d;
using namespace cocos2d::ui;

/**
 * @brief Extended cocos Button that does "blob" animations when set visible or called to animate itself
 */
class TweenButton : public Button
{
private:
    float m_delay;
    float m_duration = 0.4f;

public:
    static TweenButton *
    create(const std::string &normalImage, const std::string &selectedImage, const std::string &disableImage, float delay, bool runNow);

    bool init(const std::string &normalImage, const std::string &selectedImage, const std::string &disableImage, float delay, bool runNow);

    void animateIn();

    void setVisible(bool visible) override;
};


#endif //PROJ_ANDROID_TWEENBUTTON_H
