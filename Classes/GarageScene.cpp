//
// Created by temiv on 4/9/2019.
//

#include "GarageScene.h"
#include "VehicleConfig.h"
#include <random>

GarageScene *GarageScene::create(SceneLoader *gameState, Session *sessionData)
{
    GarageScene *scene = new(std::nothrow) GarageScene;
    if (scene && scene->init(gameState, sessionData))
    {
        scene->autorelease();
        return scene;
    }
    delete scene;
    scene = nullptr;
    return nullptr;
}

bool GarageScene::init(SceneLoader *gameState, Session *sessionData)
{
    if (!Scene::initWithPhysics())
        return false;

    m_gameState = gameState;
    m_sessionData = sessionData;
    setupUI();

    return true;
}

void GarageScene::setupUI()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();

    //Create background
    auto background = Sprite::create("background.png");
    background->setLocalZOrder(-1);
    background->setScale(1.0f);
    background->setLocalZOrder(-100);
    addChild(background);
    background->setAnchorPoint(Vec2(0.5f, 0.5f));
    background->setNormalizedPosition(Vec2(0.5f, 0.5f));

    float lowerButtonHeight = visibleSize.height * 0.33f;
    float frameHeight = visibleSize.height * 0.75f;

    TTFConfig moneyTextConfig;
    moneyTextConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    moneyTextConfig.fontSize = 30;
    moneyTextConfig.outlineSize = 2;

    //User money
    m_money = Label::createWithTTF(moneyTextConfig, "0$");
    this->addChild(m_money);
    m_money->setPosition(Vec2(visibleSize.width * 0.15f, lowerButtonHeight));
    m_money->setColor(Color3B(247, 202, 68));
    m_money->enableOutline(Color4B(99, 50, 6, 255), 1);


    m_nextButton = TweenButton::create("button_level.png", "button_level.png", "button_level.png", 0.1f, true);
    m_nextButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
        {
            //Tween out all animated elements
            m_animationGroup->AnimateSlideOut(-1);
            //Delay level load a bit so screen tweens have time to finish
            this->scheduleOnce(schedule_selector(GarageScene::loadLevelMenu), 0.5f);
        }
    });
    this->addChild(m_nextButton);
    m_nextButton->setPosition(Vec2(visibleSize.width * 0.8f, lowerButtonHeight));


    //Cheat button
    m_cheatButton = TweenButton::create("button_cheat.png", "button_cheat.png", "button_cheat.png", 0.15f, true);
    m_cheatButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
        {
            m_sessionData->addMoney(1000);
            this->updateUI();
        }
    });
    this->addChild(m_cheatButton);
    m_cheatButton->setPosition(Vec2(visibleSize.width * 0.4f, lowerButtonHeight));


    //Engine upgrade UI
    function<void()> upgradeEngine = [&]() {
        m_sessionData->upgradeEngine();
        this->updateUI();
    };
    function<void()> downgradeEngine = [&]() {
        m_sessionData->downgradeEngine();
        this->updateUI();
    };
    m_engineUpgradeFrame = UpgradeFrame::create(VehicleConfig::engineLevels[0].spriteFilePath, upgradeEngine, downgradeEngine);
    this->addChild(m_engineUpgradeFrame);
    m_engineUpgradeFrame->setPosition(Vec2(visibleSize.width * 0.2f, frameHeight));


    //Wheel upgrade UI
    function<void()> upgradeWheel = [&]() {
        m_sessionData->upgradeWheel();
        this->updateUI();
    };
    function<void()> downgradeWheel = [&]() {
        m_sessionData->downgradeWheel();
        this->updateUI();
    };
    m_wheelUpgradeFrame = UpgradeFrame::create(VehicleConfig::wheelLevels[0].spriteFilePath, upgradeWheel, downgradeWheel);
    this->addChild(m_wheelUpgradeFrame);
    m_wheelUpgradeFrame->setPosition(Vec2(visibleSize.width * 0.5f, frameHeight));


    //Suspension upgrade UI
    function<void()> upgradeSuspension = [&]() {
        m_sessionData->upgradeSuspension();
        this->updateUI();
    };
    function<void()> downgradeSuspension = [&]() {
        m_sessionData->downgradeSuspension();
        this->updateUI();
    };
    m_suspensionUpgradeFrame = UpgradeFrame::create(VehicleConfig::suspensionLevels[0].spriteFilePath, upgradeSuspension,
        downgradeSuspension);
    this->addChild(m_suspensionUpgradeFrame);
    m_suspensionUpgradeFrame->setPosition(Vec2(visibleSize.width * 0.8f, frameHeight));


    //Elements' tweening to screen
    m_animationGroup = unique_ptr<TweenGroup>(new TweenGroup(0.071f));
    m_animationGroup->addNode(m_suspensionUpgradeFrame);
    m_animationGroup->addNode(m_nextButton);
    m_animationGroup->addNode(m_wheelUpgradeFrame);
    m_animationGroup->addNode(m_cheatButton);
    m_animationGroup->addNode(m_engineUpgradeFrame);
    m_animationGroup->addNode(m_money);

    m_animationGroup->AnimateSlideIn(-1);

    this->updateUI();
}


void GarageScene::updateUI()
{
    //Money label
    auto moneyStr = std::to_string(m_sessionData->getMoney());
    moneyStr += "$";
    m_money->setString(moneyStr);


    //Engine status
    int engineLevel = m_sessionData->getEngineLevel();
    string engineSpriteName = VehicleConfig::engineLevels[engineLevel].spriteFilePath;
    m_engineUpgradeFrame->setSpriteTexture(engineSpriteName);
    m_engineUpgradeFrame->setEnableUpgrade(m_sessionData->canUpgradeEngine());
    m_engineUpgradeFrame->setEnableDowngrade(m_sessionData->canDowngradeEngine());
    m_engineUpgradeFrame->setPrice(VehicleConfig::engineLevels[engineLevel + 1].cost);

    //Don't show price if level is maxed out
    bool showEnginePrice = m_sessionData->getEngineLevel() + 1 < VehicleConfig::engineLevels.size();
    m_engineUpgradeFrame->setShowPrice(showEnginePrice);


    //Wheel status
    int wheelLevel = m_sessionData->getWheelLevel();
    string wheelSpriteName = VehicleConfig::wheelLevels[wheelLevel].spriteFilePath;
    m_wheelUpgradeFrame->setSpriteTexture(wheelSpriteName);
    m_wheelUpgradeFrame->setEnableUpgrade(m_sessionData->canUpgradeWheel());
    m_wheelUpgradeFrame->setEnableDowngrade(m_sessionData->canDowngradeWheel());
    m_wheelUpgradeFrame->setPrice(VehicleConfig::wheelLevels[wheelLevel + 1].cost);

    //Don't show price if level is maxed out
    bool showWheelPrice = m_sessionData->getWheelLevel() + 1 < VehicleConfig::wheelLevels.size();
    m_wheelUpgradeFrame->setShowPrice(showWheelPrice);


    //Suspension status
    int suspensionLevel = m_sessionData->getSuspensionLevel();
    string suspensionSpriteName = VehicleConfig::suspensionLevels[suspensionLevel].spriteFilePath;
    m_suspensionUpgradeFrame->setSpriteTexture(suspensionSpriteName);
    m_suspensionUpgradeFrame->setEnableUpgrade(m_sessionData->canUpgradeSuspension());
    m_suspensionUpgradeFrame->setEnableDowngrade(m_sessionData->canDowngradeSuspension());
    m_suspensionUpgradeFrame->setPrice(VehicleConfig::suspensionLevels[suspensionLevel + 1].cost);

    //Don't show price if level is maxed out
    bool showSuspensionPrice = m_sessionData->getSuspensionLevel() + 1 < VehicleConfig::suspensionLevels.size();
    m_suspensionUpgradeFrame->setShowPrice(showSuspensionPrice);
}

void GarageScene::loadLevelMenu(float delta)
{
    m_gameState->loadLevelMenu();
}

