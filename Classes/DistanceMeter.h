//
// Created by temiv on 4/9/2019.
//

#ifndef PROJ_ANDROID_DISTANCEMETER_H
#define PROJ_ANDROID_DISTANCEMETER_H

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;


/**
 * @brief UI element descriping player's win or lose status distance during a race
 */
class DistanceMeter : public Node
{
private:
    Sprite *m_pointer;
    Sprite *m_background;
    Sprite *m_warningBackground;
    float m_val;
    float m_flashTimer;

public:
    CREATE_FUNC(DistanceMeter);

    bool init() override;

    void setValue(float valueNormalized);

    void update(float delta) override;

};


#endif //PROJ_ANDROID_DISTANCEMETER_H
