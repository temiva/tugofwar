//
// Created by temiv on 4/3/2019.
//

#include "Wheel.h"
#include "VehicleBuildHelper.h"


Wheel *Wheel::create(Scene &scene, string filename, float radius, float brakePower, Vec2 position, bool isDriveWheel,
    PhysicsMaterial physicsMaterial, int categoryBitmask, int collisionBitmask)
{

    Wheel *wheel = new(std::nothrow) Wheel;
    if (wheel &&
        wheel->init(scene, filename, radius, brakePower, position, isDriveWheel, physicsMaterial, categoryBitmask, collisionBitmask))
    {
        wheel->autorelease();
        return wheel;
    }
    delete wheel;
    wheel = nullptr;
    return nullptr;
}

bool Wheel::init(Scene &scene, string filename, float radius, float brakePower, Vec2 position, bool isDriveWheel,
    PhysicsMaterial physicsMaterial, int categoryBitmask, int collisionBitmask)
{

    if (!Node::init())
        return false;

    m_sprite = VehicleBuildHelper::createCircleBody(scene, radius, position, Vec2(0.0f, 0.0f), filename, physicsMaterial, categoryBitmask,
        collisionBitmask);
    m_body = m_sprite->getPhysicsBody();
    m_isDriveWheel = isDriveWheel;
    m_brakePower = brakePower;
    m_body->setAngularVelocityLimit(999999);
    m_body->setAngularDamping(150.0f);

    this->addChild(m_sprite);
    this->scheduleUpdate();

    return true;
}

void Wheel::update(float dt)
{
    if (m_isDriveWheel)
        m_body->applyTorque(m_torque);
}

void Wheel::setBrake(bool brake)
{
    //Increase rotation resistance significantly if brake is on
    m_body->setAngularDamping(brake ? 2500000.0f * m_brakePower : 0.475f);

    //Stop rotation
    m_body->setAngularVelocity(0);

    //Limit angular velocity
    m_body->setAngularVelocityLimit(brake ? 0.0f : 9999999);
}

void Wheel::setTorque(float torque)
{
    m_torque = torque;
}
