//
// Created by temiv on 4/9/2019.
//

#include "DistanceMeter.h"
#include <tgmath.h>


bool DistanceMeter::init()
{
    if (!Node::init())
        return false;

    m_background = Sprite::create("progress_meter.png");
    m_background->setAnchorPoint(Vec2(0.5f, 0.5f));
    this->addChild(m_background);

    m_warningBackground = Sprite::create("progress_meter_warning.png");
    m_warningBackground->setPosition(Vec2(0, 0));
    m_warningBackground->setAnchorPoint(Vec2(0.5f, 0.5f));
    this->addChild(m_warningBackground);

    m_pointer = Sprite::create("progress_pointer.png");
    m_pointer->setAnchorPoint(Vec2(0.5f, 0.75f));
    this->addChild(m_pointer);

    this->scheduleUpdate();

    return true;
}

void DistanceMeter::setValue(float valueNormalized)
{
    m_val = valueNormalized;
    Vec2 position = Vec2(m_background->getContentSize().width * 0.5 * valueNormalized, -m_background->getContentSize().height * 0.5f);
    m_pointer->setPosition(position);
}

void DistanceMeter::update(float dt)
{
    m_flashTimer += dt;
    if (m_warningBackground && m_val < -0.05f)
    {
        float sinState = (sin(m_flashTimer * 10.0f) + 1.0f) * 0.5f;
        m_warningBackground->setOpacity(sinState * 255);
    }
    else
        m_warningBackground->setOpacity(0);
}