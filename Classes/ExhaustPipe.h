//
// Created by temiv on 4/10/2019.
//

#ifndef PROJ_ANDROID_EXHAUSTPIPE_H
#define PROJ_ANDROID_EXHAUSTPIPE_H

#include <string>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

class ExhaustPipe : public Node
{
private:
    Sprite *m_sprite;

    ParticleSystemQuad *m_smokeParticle = nullptr;
    float m_defaultSmokeEmissionRate;
    float m_defaultSmokeEmissionSpeed;
    float m_defaultSmokeStartSize;
    float m_defaultSmokeEndSize;
    Color4F m_defaultSmokeStartColor;
    Color4F m_defaultSmokeEndColor;

    ParticleSystemQuad *m_flameParticle = nullptr;
    float m_defaultFlameEmissionRate;
    float m_defaultFlameEmissionSpeed;
    float m_defaultFlameStartSize;
    float m_defaultFlameLife;

    void setSmokeParticle(ParticleSystemQuad *particleSystem);

    void setFlameParticle(ParticleSystemQuad *particleSystem);

public:
    /**
     * @brief Creates an exhaust pipe node
     * @param textureFile sprite texture path
     * @param smokeParticleFile path to smoke particle. Emission / visibility is adjusted when gas value is set
     * @param flameParticleFile path to flame particle. Will emit when temperature is set to high enough value.
     */
    static ExhaustPipe *create(string textureFile, string smokeParticleFile, string flameParticleFile);

    bool init(string textureFile, string smokeParticleFile, string flameParticleFile);

    /**
     * @param val between 0 and 1. High value will emit more smoke
     */
    void setGasValue(float val);

    /**
     * @param val between 0 and 1. High value will make exhaust emit flame particles
     */
    void setTemperature(float val);
};


#endif //PROJ_ANDROID_EXHAUSTPIPE_H
