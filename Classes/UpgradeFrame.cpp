//
// Created by temiv on 4/13/2019.
//

#include "UpgradeFrame.h"

UpgradeFrame *UpgradeFrame::create(const string &previewSprite, function<void()> upgradeFunction, function<void()> downgradeFunction)
{
    UpgradeFrame *frame = new(std::nothrow) UpgradeFrame;
    if (frame && frame->init(previewSprite, upgradeFunction, downgradeFunction))
    {
        frame->autorelease();
        return frame;
    }
    delete frame;
    frame = nullptr;
    return nullptr;
}

bool UpgradeFrame::init(const string &previewSpriteFile, function<void()> upgradeFunction, function<void()> downgradeFunction)
{
    if (!Node::init())
        return false;

    this->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_upgradeFunction = upgradeFunction;
    m_downgradeFunction = downgradeFunction;

    //Frame
    auto frame = Sprite::create("frame_part.png");
    this->addChild(frame);
    Size frameSize = frame->getContentSize();

    //Preview sprite
    m_previewSprite = Sprite::create(previewSpriteFile);
    this->addChild(m_previewSprite);
    m_previewSprite->setScale(1.35f);

    //Upgrade button
    m_upgradeButton = Button::create("button_upgrade.png", "button_upgrade.png", "button_upgrade_greyed.png");
    this->addChild(m_upgradeButton);
    m_upgradeButton->setPosition(Vec2(frameSize.width * 0.0f, frameSize.height * 0.5f));
    m_upgradeButton->setScale(0.8f);
    m_upgradeButton->addTouchEventListener(CC_CALLBACK_2(UpgradeFrame::onUpgradeButton, this));

    //Downgrade button
    m_downgradeButton = Button::create("button_downgrade.png", "button_downgrade.png", "button_downgrade_greyed.png");
    this->addChild(m_downgradeButton);
    m_downgradeButton->setPosition(Vec2(frameSize.width * 0.0f, frameSize.height * -0.5f));
    m_downgradeButton->setScale(0.8f);
    m_downgradeButton->addTouchEventListener(CC_CALLBACK_2(UpgradeFrame::onDowngradeButton, this));

    TTFConfig priceTextConfig;
    priceTextConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    priceTextConfig.fontSize = 18;
    priceTextConfig.outlineSize = 1;

    //Engine cost label
    m_priceLabel = Label::createWithTTF(priceTextConfig, "0$");
    this->addChild(m_priceLabel);
    m_priceLabel->setPosition(Vec2(frameSize.width * -0.4f, frameSize.height * 0.5f));
    m_priceLabel->enableOutline(Color4B(44, 44, 44, 255), 1);

    return true;
}

void UpgradeFrame::onUpgradeButton(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        if (m_upgradeFunction)
            m_upgradeFunction();
    }
}

void UpgradeFrame::onDowngradeButton(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        if (m_downgradeFunction)
            m_downgradeFunction();
    }
}

void UpgradeFrame::setSpriteTexture(const string &spriteFilename)
{
    if (m_previewSprite)
        m_previewSprite->setTexture(spriteFilename);
}

void UpgradeFrame::setEnableUpgrade(bool enable)
{
    if (m_upgradeButton)
        m_upgradeButton->setBright(enable);

    if (m_priceLabel)
        m_priceLabel->setColor(enable ? Color3B::WHITE : Color3B(225, 100, 90));
}

void UpgradeFrame::setEnableDowngrade(bool enable)
{
    if (m_downgradeButton)
        m_downgradeButton->setBright(enable);
}

void UpgradeFrame::setPrice(int price)
{
    if (m_priceLabel)
    {
        string priceStr = to_string(price);
        priceStr += "$";
        m_priceLabel->setString(priceStr);
    }
}

void UpgradeFrame::setShowPrice(bool show)
{
    if (m_priceLabel)
    {
        m_priceLabel->setVisible(show);
    }
}