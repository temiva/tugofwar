//
// Created by temiv on 4/5/2019.
//

#ifndef PROJ_ANDROID_AICONTROLLER_H
#define PROJ_ANDROID_AICONTROLLER_H

#include "cocos2d.h"
#include "Vehicle.h"

enum AIState
{
    Accelerate, Brake, Idle //Do nothing, way to adjust difficulty
};

/**
 * @brief Very rudimentary AI logic. Simply selects randomly between accelerating, idling and braking
 */
class AIController : public Node
{
private:
    AIState state = AIState::Accelerate;
    Vehicle *m_vehicle;
    float m_nextTempTarget = 0;
    float m_idleTime = 0;
    float m_idleTimeTarget = 0;
    float m_targetGasLevel = 0;

    VehicleInputValue getInput();

    void UpdateState(float dt);

    void SetIdleState();

    void SetAccelerateState();

    void SetBrakeState();

public:
    /**
     * @param vehicle vehicle that this AI will control
     */
    static AIController *create(Vehicle *vehicle);

    bool init(Vehicle *vehicle);

    void update(float delta) override;
};


#endif //PROJ_ANDROID_AICONTROLLER_H
