//
// Created by temiv on 4/8/2019.
//

#ifndef PROJ_ANDROID_GAMESTATE_H
#define PROJ_ANDROID_GAMESTATE_H

#include "cocos2d.h"
#include "Session.h"

using namespace cocos2d;
using namespace std;

/**
 * @brief Main scene orchestrator. Switches between game and menu scenes
 */
class SceneLoader
{
private:
    unique_ptr<Session> m_sessionData;

public:
    SceneLoader();

    ~SceneLoader();

    void restart();

    void loadCarMenu();

    void loadLevelMenu();

    void loadRaceScene();
};


#endif //PROJ_ANDROID_GAMESTATE_H
