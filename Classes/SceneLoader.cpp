//
// Created by temiv on 4/8/2019.
//


#include "SceneLoader.h"
#include "RaceScene.h"
#include "GarageScene.h"
#include "OpponentSelectScene.h"

SceneLoader::SceneLoader()
{
    m_sessionData = unique_ptr<Session>(new Session(100, 0, 0, 0));

    auto director = Director::getInstance();
    auto firstScene = GarageScene::create(this, m_sessionData.get());
    //auto firstScene = RaceScene::createScene(this, m_sessionData.get());
    director->runWithScene(firstScene);
}

SceneLoader::~SceneLoader()
{
    m_sessionData = nullptr;
}

void SceneLoader::restart()
{
    loadRaceScene();
}

void SceneLoader::loadCarMenu()
{
    auto director = Director::getInstance();
    auto scene = GarageScene::create(this, m_sessionData.get());
    director->replaceScene(scene);
}

void SceneLoader::loadLevelMenu()
{
    auto director = Director::getInstance();
    auto scene = OpponentSelectScene::create(this, m_sessionData.get());
    director->replaceScene(scene);
}

void SceneLoader::loadRaceScene()
{
    auto director = Director::getInstance();
    auto scene = RaceScene::create(this, m_sessionData.get());
    director->replaceScene(scene);
}