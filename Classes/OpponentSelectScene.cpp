//
// Created by temiv on 4/11/2019.
//

#include "OpponentSelectScene.h"

static const int maxLevel = 2;
static const int minLevel = 0;

OpponentSelectScene *OpponentSelectScene::create(SceneLoader *gameState, Session *sessionData)
{
    OpponentSelectScene *scene = new(std::nothrow) OpponentSelectScene;
    if (scene && scene->init(gameState, sessionData))
    {
        scene->autorelease();
        return scene;
    }
    delete scene;
    scene = nullptr;
    return nullptr;
}


bool OpponentSelectScene::init(SceneLoader *gameState, Session *sessionData)
{
    if (!OpponentSelectScene::initWithPhysics())
        return false;
    m_gameState = gameState;
    m_sessionData = sessionData;

    auto groundBody = PhysicsBody::createBox(Size(100000, 130));
    groundBody->setDynamic(false);
    //Any other way to create invisible physics body???
    auto groundSprite = Sprite::create("transparent.png");
    groundSprite->addComponent(groundBody);
    addChild(groundSprite);
    groundSprite->setPosition((Vec2(0, 45)));

    getPhysicsWorld()->setGravity(Vec2(0, -80));
    getPhysicsWorld()->setSubsteps(10);
    getPhysicsWorld()->setFixedUpdateRate(120);
    //getPhysicsWorld()->setDebugDrawMask(0xffff);

    randomizeEnemy(0);
    SetupUI();

    return true;
}

const std::vector<VehiclePartConfig> configs[] = {VehicleConfig::truckBackFrames, VehicleConfig::truckBuckets,
                                                  VehicleConfig::truckCabinFronts, VehicleConfig::truckCabins, VehicleConfig::truckSteppers,
                                                  VehicleConfig::truckHoods, VehicleConfig::truckDoors, VehicleConfig::truckBackFenders,
                                                  VehicleConfig::truckFrontFenders,};

void OpponentSelectScene::randomizeEnemy(int difficultyLevel)
{
    //Remove old preview
    if (m_enemyPreview)
        this->removeChild(m_enemyPreview, true);

    int engineLevel = difficultyLevel;
    int wheelLevel = difficultyLevel;
    int suspensionLevel = difficultyLevel;

    //add some random variation to opponents at higher difficulty levels
    if (difficultyLevel >= 0)
    {
        engineLevel += (rand() % 2 - 1);
        wheelLevel += (rand() % 2 - 1);
        suspensionLevel += (rand() % 2 - 1);
    }

    auto vehicleSetup = VehicleSetup(engineLevel, wheelLevel, suspensionLevel);

    //Randomize cosmetic parts
    vector<VehiclePartConfig> vehicleCosmetics;

    //Random color selection. A little higher value than there are color variations to get more rust values ( currently the last variation)
    int spriteTheme = rand() % 6;

    for (auto configOptions : configs)
    {
        //Chance for part to be missing completely
        if (rand() % 10 == 1)
            continue;

        //Chance for multi color truck theme. Reshuffle color theme
        if (rand() % 15 == 1)
            spriteTheme = rand() % 7;

        auto config = configOptions[rand() % configOptions.size()];
        config.selectSpriteVariation(spriteTheme);
        vehicleCosmetics.push_back(config);
    }

    vehicleSetup.setCosmetics(vehicleCosmetics);
    m_sessionData->setEnemyVehicleSetup(vehicleSetup);
    m_enemyPreview = Vehicle::create(&vehicleSetup, *this, Vec2(230, 200), false);
}


void OpponentSelectScene::SetupUI()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();

    auto background = Sprite::create("background.png");
    background->setLocalZOrder(-1);
    background->setScale(1.0f);
    background->setLocalZOrder(-100);
    addChild(background);
    background->setAnchorPoint(Vec2(0.5f, 0.5f));
    background->setNormalizedPosition(Vec2(0.5f, 0.5f));


    TTFConfig labelConfig;
    labelConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    labelConfig.fontSize = 18;
    labelConfig.outlineSize = 1;


    //Start race button
    m_raceButton = TweenButton::create("button_race.png", "button_race.png", "button_race.png", 0.1f, true);
    m_raceButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
            m_gameState->loadRaceScene();
    });
    this->addChild(m_raceButton);
    m_raceButton->setPosition(Vec2(visibleSize.width - m_raceButton->getContentSize().width * 0.6f, visibleSize.height * 0.3f));

    //Back to garage button
    m_backButton = TweenButton::create("button_back.png", "button_back.png", "button_back.png", 0.15f, true);
    m_backButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
            m_gameState->loadCarMenu();
    });
    this->addChild(m_backButton);
    m_backButton->setPosition(Vec2(0 + m_backButton->getContentSize().width * 0.6f, visibleSize.height * 0.3f));


    //Previous enemy level button
    m_prevLevelButton = TweenButton::create("button_arrow_left.png", "button_arrow_left.png", "button_arrow_left_disabled.png", 0.2f, true);
    m_prevLevelButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
        {
            m_enemyLevel--;
            if (m_enemyLevel < minLevel)
                m_enemyLevel = minLevel;
            randomizeEnemy(m_enemyLevel);
            UpdateUI();
        }
    });
    this->addChild(m_prevLevelButton);
    m_prevLevelButton->setPosition(Vec2(visibleSize.width * 0.1f, visibleSize.height * 0.7f));


    //Next enemy level button
    m_nextLevelButton = TweenButton::create("button_arrow_right.png", "button_arrow_right.png", "button_arrow_right_disabled.png", 0.25f,
        true);
    m_nextLevelButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        if (type == Widget::TouchEventType::ENDED)
        {
            m_enemyLevel++;
            if (m_enemyLevel > maxLevel)
                m_enemyLevel = maxLevel;
            randomizeEnemy(m_enemyLevel);
            UpdateUI();
        }
    });
    this->addChild(m_nextLevelButton);
    m_nextLevelButton->setPosition(Vec2(visibleSize.width * 0.9f, visibleSize.height * 0.7f));

    TTFConfig headerTextConfig;
    headerTextConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    headerTextConfig.fontSize = 30;
    headerTextConfig.outlineSize = 2;

    //engine cost
    m_headerLabel = Label::createWithTTF(headerTextConfig, "0$");
    this->addChild(m_headerLabel);
    m_headerLabel->setNormalizedPosition(Vec2(0.5f, 0.75f));
    m_headerLabel->setColor(Color3B(233, 233, 233));
    m_headerLabel->enableOutline(Color4B(99, 50, 6, 255), 1);
    m_headerLabel->setString("Select challenger");

    TTFConfig levelTextConfig;
    levelTextConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    levelTextConfig.fontSize = 24;
    levelTextConfig.outlineSize = 2;

    //engine cost
    m_rewardLabel = Label::createWithTTF(levelTextConfig, "0$");
    this->addChild(m_rewardLabel);
    m_rewardLabel->setNormalizedPosition(Vec2(0.5f, 0.25f));
    m_rewardLabel->setColor(Color3B(247, 202, 68));
    m_rewardLabel->enableOutline(Color4B(99, 50, 6, 255), 1);


    m_animationGroup = unique_ptr<TweenGroup>(new TweenGroup(0.071f));
    m_animationGroup->addNode(m_raceButton);
    m_animationGroup->addNode(m_nextLevelButton);
    m_animationGroup->addNode(m_backButton);
    m_animationGroup->addNode(m_prevLevelButton);
    m_animationGroup->AnimateSlideIn(-1);

    this->UpdateUI();
}

void OpponentSelectScene::UpdateUI()
{
    int moneyReward = m_sessionData->getEnemyVehicleSetup()->getReward();
    string rewardStr = "";
    if (m_enemyLevel == 0)
        rewardStr += "EASY\n";
    else if (m_enemyLevel == 1)
        rewardStr += "MEDIUM\n";
    else if (m_enemyLevel == 2)
        rewardStr += "HARD!\n";

    rewardStr += std::to_string(moneyReward);
    rewardStr += "$";
    m_rewardLabel->setString(rewardStr);

    m_nextLevelButton->setBright(m_enemyLevel != maxLevel);
    m_prevLevelButton->setBright(m_enemyLevel != minLevel);
}
