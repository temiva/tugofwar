//
// Created by temiv on 4/3/2019.
//

#include "RaceUILayer.h"
#include <string>
#include "VehicleInput.h"
#include "Vehicle.h"
#include "Race.h"


RaceUILayer *RaceUILayer::create(SceneLoader *gameState)
{
    RaceUILayer *touchLayer = new(std::nothrow) RaceUILayer;
    if (touchLayer && touchLayer->init(gameState))
    {
        touchLayer->autorelease();
        return touchLayer;
    }
    delete touchLayer;
    touchLayer = nullptr;
    return nullptr;
}

bool RaceUILayer::init(SceneLoader *gameState)
{
    if (!Layer::init())
        return false;

    m_gameState = gameState;
    setupUIElements();
    setupEventListeners();

    return true;
}


void RaceUILayer::setupUIElements()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();

    //Gas label style
    TTFConfig gasLabelConfig;
    gasLabelConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    gasLabelConfig.fontSize = 24;
    gasLabelConfig.outlineSize = 1;

    //Time label style
    TTFConfig timeLabelConfig;
    timeLabelConfig.fontFilePath = "LondrinaSolid-Regular.ttf";
    timeLabelConfig.fontSize = 32;
    timeLabelConfig.outlineSize = 1;

    //Game time label
    m_timeLabel = Label::createWithTTF(timeLabelConfig, "3");
    m_timeLabel->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.9f));
    m_timeLabel->enableOutline(Color4B(44, 44, 44, 255), 1);
    this->addChild(m_timeLabel);

    //Temperature gauge ui
    m_gauge = Gauge::create();
    m_gauge->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.32f));
    this->addChild(m_gauge);

    //Player's distance meter
    m_distanceMeter = DistanceMeter::create();
    m_distanceMeter->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 1.05f));
    this->addChild(m_distanceMeter);

    //Engine destroyed text
    m_vehicleStatus = Sprite::create("text_engine_destroyed.png");
    m_vehicleStatus->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.68f));
    m_vehicleStatus->setVisible(false);
    this->addChild(m_vehicleStatus);

    //Lose texts
    m_loseSprite = Sprite::create("text_lose.png");
    m_loseSprite->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.82f));
    m_loseSprite->setVisible(false);
    this->addChild(m_loseSprite);

    //Win text
    m_winSprite = Sprite::create("text_win.png");
    m_winSprite->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.8f));
    m_winSprite->setVisible(false);
    this->addChild(m_winSprite);

    //"You" player pointer sprite
    auto youSprite = Sprite::create("pointer_you.png");
    youSprite->setPosition(Vec2(visibleSize.width * 0.65f, visibleSize.height * 0.85f));
    this->addChild(youSprite);

    //Tween out pointer after a while
    auto fadeOutSequence = Sequence::create(DelayTime::create(5.0f), FadeOut::create(0.5f), nullptr);
    youSprite->runAction(RepeatForever::create(fadeOutSequence));

    //Restart button
    m_restartButton = TweenButton::create("button_restart.png", "button_restart.png", "button_restart.png", 0.1f, false);
    m_restartButton->setPosition(
        Vec2(visibleSize.width * 0.5f + m_restartButton->getContentSize().width * 0.6f, visibleSize.height * 0.4f));
    m_restartButton->addTouchEventListener(CC_CALLBACK_2(RaceUILayer::onRestartButton, this));
    m_restartButton->setVisible(false);
    this->addChild(m_restartButton);

    //Garage button
    m_garageButton = TweenButton::create("button_garage.png", "button_garage.png", "button_garage.png", 0.15f, false);
    m_garageButton->setPosition(Vec2(visibleSize.width * 0.5f - m_garageButton->getContentSize().width * 0.6f, visibleSize.height * 0.4f));
    m_garageButton->addTouchEventListener(CC_CALLBACK_2(RaceUILayer::onGarageButton, this));
    m_garageButton->setVisible(false);
    this->addChild(m_garageButton);

    //Brake input circle button
    m_brakeInputSprite = Sprite::create("input_brake.png");
    m_brakeInputSprite->setPosition(Vec2(visibleSize.width * 0.1f, visibleSize.height * 0.35f));
    m_brakeInputSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_brakeDefaultPosition = m_brakeInputSprite->getPosition();
    this->addChild(m_brakeInputSprite);

    //Gas input line
    auto gasInputLine = Sprite::create("input_gas_line.png");
    gasInputLine->setPosition(Vec2(visibleSize.width * 0.9f, visibleSize.height * 0.65f));
    this->addChild(gasInputLine);

    //Gas input text
    m_labelTouchInfo = Label::createWithTTF(gasLabelConfig, "0%");
    m_labelTouchInfo->setPosition(Vec2(visibleSize.width * 0.9f, visibleSize.height * 0.91f));
    m_labelTouchInfo->setAnchorPoint(Vec2(0.5f, 0.0f));
    m_labelTouchInfo->setColor(Color3B::WHITE);
    m_labelTouchInfo->enableOutline(Color4B(44, 44, 44, 255), 1);
    this->addChild(m_labelTouchInfo);

    //Gas input circle button
    m_gasInputSprite = Sprite::create("input_gas.png");
    m_gasInputSprite->setPosition(Vec2(visibleSize.width * 0.9f, visibleSize.height * 0.35f));
    m_brakeInputSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
    this->addChild(m_gasInputSprite);
}

void RaceUILayer::setupEventListeners()
{
    //Touch events
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(RaceUILayer::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(RaceUILayer::onTouchEnded, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(RaceUILayer::onTouchMoved, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(RaceUILayer::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    //Player's vehicle update events
    EventListenerCustom *vehicleListener = EventListenerCustom::create(Vehicle::EVT_PLAYER_VEHICLE_UPDATED,
        CC_CALLBACK_1(RaceUILayer::playerVehicleUpdatedHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(vehicleListener, this);

    //Race countdown timer event
    EventListenerCustom *raceCountdownListener = EventListenerCustom::create(Race::EVT_RACE_COUNTDOWN_SET,
        CC_CALLBACK_1(RaceUILayer::countdownSetHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceCountdownListener, this);

    //Race time updated event
    EventListenerCustom *raceTimeSetListener = EventListenerCustom::create(Race::EVT_RACE_TIME_SET,
        CC_CALLBACK_1(RaceUILayer::raceTimeSetHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceTimeSetListener, this);

    //Race started event
    EventListenerCustom *raceStartedListener = EventListenerCustom::create(Race::EVT_RACE_STARTED,
        CC_CALLBACK_1(RaceUILayer::raceStartedHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceStartedListener, this);

    //Race won
    EventListenerCustom *raceWonListener = EventListenerCustom::create(Race::EVT_RACE_WON,
        CC_CALLBACK_1(RaceUILayer::raceWonHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceWonListener, this);

    //Race lost
    EventListenerCustom *raceLostListener = EventListenerCustom::create(Race::EVT_RACE_LOST,
        CC_CALLBACK_1(RaceUILayer::raceLostHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceLostListener, this);

    //Race ended (win or lose)
    EventListenerCustom *raceEndedListener = EventListenerCustom::create(Race::EVT_RACE_ENDED,
        CC_CALLBACK_1(RaceUILayer::raceEndedHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceEndedListener, this);
}

void RaceUILayer::playerVehicleUpdatedHandler(EventCustom *event)
{
    Vehicle *vehicle = static_cast<Vehicle *>(event->getUserData());

    //Convert from pixels to "meters" (not real meters since art was not made to scale, just eyeballed)
    int distance = (int) (vehicle->getPosition().x / 60);
    m_gauge->setValue(vehicle->getEngine()->getTemperature());
    m_distanceMeter->setValue(vehicle->getPosition().x / Race::DISTANCE_THRESSHOLD);

    //Engine busted -> show corresponding text to user
    if (vehicle->getEngine()->getDamage() >= 1.0f)
    {
        m_vehicleStatus->setVisible(true);
        m_restartButton->setVisible(true);
        m_garageButton->setVisible(true);
    }
}

void RaceUILayer::countdownSetHandler(EventCustom *event)
{
    int *countdown = static_cast<int *>(event->getUserData());
    m_timeLabel->setString(std::to_string(*countdown));

    //Scale down and fade in countdown number
    auto scale = ScaleTo::create(0.4f, 1.75f);
    auto fadeIn = FadeIn::create(0.4f);
    auto scaleEased = EaseIn::create(scale->clone(), 1.0f);
    auto fadeInEased = EaseIn::create(fadeIn->clone(), 1.0f);

    m_timeLabel->setScale(12.0f);
    m_timeLabel->setOpacity(0);
    m_timeLabel->runAction(scaleEased);
    m_timeLabel->runAction(fadeInEased);
}

void RaceUILayer::raceStartedHandler(EventCustom *event)
{
    m_timeLabel->setString("GO!");
    auto scale = ScaleTo::create(0.4f, 1.75f);
    auto fadeIn = FadeIn::create(0.4f);
    auto scaleEased = EaseIn::create(scale->clone(), 2.0f);
    auto fadeInEased = EaseIn::create(fadeIn->clone(), 1.0f);

    //Scale down and fade in a go command
    m_timeLabel->setScale(12.0f);
    m_timeLabel->setOpacity(0);
    m_timeLabel->runAction(scaleEased);
    m_timeLabel->runAction(fadeInEased);
}

void RaceUILayer::raceTimeSetHandler(EventCustom *event)
{
    m_raceTimeElapsed++;
    if (m_raceTimeElapsed > 3.0f)
    {
        m_timeLabel->setScale(1.0f);
        int *timeLeft = static_cast<int *>(event->getUserData());
        m_timeLabel->setString(std::to_string(*timeLeft));
    }
}

void RaceUILayer::raceWonHandler(EventCustom *event)
{
    auto scaleTo = ScaleTo::create(0.6f, 1.0f);
    auto fadeIn = FadeIn::create(0.6f);
    m_garageButton->setVisible(true);
    m_winSprite->setVisible(true);
    m_winSprite->setOpacity(0);
    m_winSprite->setScale(14.0f);
    m_winSprite->runAction(scaleTo);
    m_winSprite->runAction(fadeIn);
}

void RaceUILayer::raceLostHandler(EventCustom *event)
{
    //Scale down and fade in "You lost"
    auto scaleTo = ScaleTo::create(0.6f, 1.0f);
    auto fadeIn = FadeIn::create(0.6f);
    m_loseSprite->setVisible(true);
    m_loseSprite->setOpacity(0);
    m_loseSprite->setScale(14.0f);
    m_loseSprite->runAction(scaleTo);
    m_loseSprite->runAction(fadeIn);

    m_restartButton->setVisible(true);
    m_garageButton->setVisible(true);
}

void RaceUILayer::raceEndedHandler(EventCustom *event)
{
    m_distanceMeter->setVisible(false);
    m_gauge->setVisible(false);
    m_timeLabel->setVisible(false);
    m_gasInputSprite->setVisible(false);
    m_brakeInputSprite->setVisible(false);
}

float RaceUILayer::getPedalMinHeight()
{
    float height = Director::getInstance()->getVisibleSize().height;
    return height * m_pedalPaddingBottom;
}

float RaceUILayer::getPedalMaxHeight()
{
    float height = Director::getInstance()->getVisibleSize().height;
    return height - height * m_pedalPaddingTop;
}

void RaceUILayer::updateTouchInput(Touch *touch, bool touchReleased)
{
    //Gas input on right side of screen
    if (touch->getLocation().x > Director::getInstance()->getVisibleSize().width * 0.5f)
    {
        //"normalize" between 0 and 1 based in min and max pedal height
        m_gasInput = 1.0f - ((touch->getLocation().y - getPedalMinHeight()) / (getPedalMaxHeight()));
        m_gasInput = min(1.0f, max(m_gasInput, 0.0f)); //clamp to 0-1

        m_brakeInput = false;
        if (touchReleased)
            m_gasInput = 0.0f;
    }
    else
    {
        m_brakeInput = true;
        m_gasInput = 0.0f;
        if (touchReleased)
        {
            m_brakeInput = false;
            m_brakeInputSprite->setPosition(m_brakeDefaultPosition);
        }
        else
            m_brakeInputSprite->setPosition(touch->getLocation());
    }
    m_brakeInputSprite->setTexture(m_brakeInput ? "input_brake_lit.png" : "input_brake.png");
    m_gasInputSprite->setTexture(m_gasInput > 0.001 ? "input_gas_lit.png" : "input_gas.png");

    m_gasInput = min(1.0f, max(m_gasInput, 0.0f)); //clamp to 0-1
    setGasPedalPosition(m_gasInput);
    VehicleInputValue input = {m_gasInput, m_brakeInput};
    EventCustom event(VehicleInput::EVT_USER_INPUT_SET);
    event.setUserData(&input);
    _eventDispatcher->dispatchEvent(&event);
}

void RaceUILayer::resetGasInput()
{
    m_gasInput = 0;
    setGasPedalPosition(0);
}

void RaceUILayer::setGasPedalPosition(float gasLevel)
{
    float screenWidth = Director::getInstance()->getVisibleSize().width;
    float yPos = getPedalMinHeight() + (1.0f - gasLevel) * getPedalMaxHeight();

    string gasValueStr = "Max";
    if (gasLevel < 0.999f)
    {
        gasValueStr = std::to_string((int) (gasLevel * 100));
        gasValueStr += "%";
    }
    m_labelTouchInfo->setString(gasValueStr);
    m_gasInputSprite->setPosition(Vec2(m_gasInputSprite->getPosition().x, yPos));
}

bool RaceUILayer::onTouchBegan(Touch *touch, Event *event)
{
    updateTouchInput(touch, false);
    return true;
}

void RaceUILayer::onTouchEnded(Touch *touch, Event *event)
{
    resetGasInput();
    updateTouchInput(touch, true);
}

void RaceUILayer::onTouchMoved(Touch *touch, Event *event)
{
    updateTouchInput(touch, false);
}

void RaceUILayer::onTouchCancelled(Touch *touch, Event *event)
{
    resetGasInput();
    updateTouchInput(touch, true);
}

void RaceUILayer::onRestartButton(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
        m_gameState->restart();
}

void RaceUILayer::onGarageButton(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
        m_gameState->loadCarMenu();
}