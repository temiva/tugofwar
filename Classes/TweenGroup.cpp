//
// Created by temiv on 4/12/2019.
//

#include "TweenGroup.h"

void TweenGroup::AnimateSlideIn(int dir)
{
    if (m_nodes.size() <= 0)
        return;

    //Animate in nodes in order they were added
    for (int i = 0; i < m_nodes.size(); i++)
    {
        Node *node = m_nodes[i];

        Vec2 targetPos = node->getPosition();
        Vec2 startPos = targetPos;
        startPos.x += dir * 600;

        node->setPosition(startPos);
        node->setScale(2.0f, 0.6f);
        float randomDelay = m_delayBetweenElements * (((rand() % 1000) * 0.001f) - 0.5f) * 0.3f;
        auto moveTo = MoveTo::create(m_duration, targetPos);
        auto scaleTo = ScaleTo::create(0.3f, 1.0f);
        auto delay = DelayTime::create(i * m_delayBetweenElements + randomDelay);
        auto moveEased = EaseIn::create(moveTo->clone(), 2.0f);
        auto scaleToEased = EaseElasticOut::create(scaleTo->clone());

        auto sequence = Sequence::create(delay, moveEased, scaleToEased, nullptr);
        node->runAction(RepeatForever::create(sequence));
    }
}

void TweenGroup::AnimateSlideOut(int dir)
{
    if (m_nodes.size() <= 0)
        return;

    //Animate out nodes in reverse order
    for (int i = m_nodes.size() - 1; i >= 0; i--)
    {
        Node *node = m_nodes[i];

        Vec2 startPos = node->getPosition();
        Vec2 targetPos = startPos;
        targetPos.x -= dir * 600;

        node->setPosition(startPos);
        node->setScale(1.1f, 0.9f);
        float randomDelay = 0.5f * m_delayBetweenElements * (((rand() % 1000) * 0.001f) - 0.5f) * 0.3f;
        auto moveTo = MoveTo::create(m_duration * 0.5f, targetPos);
        auto delay = DelayTime::create(i * m_delayBetweenElements * 0.5f + randomDelay);
        auto moveEased = EaseIn::create(moveTo->clone(), 2.0f);

        auto sequence = Sequence::create(delay, moveEased, nullptr);
        node->runAction(RepeatForever::create(sequence));
    }
}
