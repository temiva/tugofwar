//
// Created by temiv on 4/3/2019.
//

#include "Vehicle.h"
#include "VehicleBuildHelper.h"
#include "Race.h"
#include "VehicleConfig.h"
#include <random>

const string Vehicle::EVT_PLAYER_VEHICLE_UPDATED = "EVT_PLAYER_VEHICLE_UPDATED";
const string Vehicle::EVT_AI_VEHICLE_UPDATED = "EVT_AI_VEHICLE_UPDATED";

int makeCollisionBitmask(bool staticWorld, bool playerCar, bool enemyCar)
{
    int mask = 0;
    mask |= (staticWorld ? 1 : 0) << 0;
    mask |= (playerCar ? 1 : 0) << 1;
    mask |= (enemyCar ? 1 : 0) << 2;
    return mask;
}

Vehicle *Vehicle::create(VehicleSetup *vehicleSetup, Scene &scene, Vec2 spawnPosition, bool playerControlled)
{
    Vehicle *vehicle = new(std::nothrow) Vehicle;
    if (vehicle && vehicle->init(vehicleSetup, scene, spawnPosition, playerControlled))
    {
        vehicle->autorelease();
        return vehicle;
    }

    delete vehicle;
    vehicle = nullptr;
    return nullptr;
}

bool Vehicle::init(VehicleSetup *vehicleSetup, Scene &scene, Vec2 spawnPosition, bool playerControlled)
{
    if (!Node::init())
        return false;

    scene.addChild(this);
    this->setPosition(spawnPosition);

    //Collide with world and opponent but not self
    this->m_playerControlled = playerControlled;
    int categoryCarBody = makeCollisionBitmask(false, playerControlled, !playerControlled);
    int colCarBody = makeCollisionBitmask(true, !playerControlled, playerControlled);
    int noCol = makeCollisionBitmask(false, false, false);

    bool flipX = playerControlled;
    int flipSign = flipX ? -1 : 1;

    //Vehicle Shadow
    m_shadow = Sprite::create("shadow.png");
    this->addChild(m_shadow);

    //Car body / center
    auto centerConfig = VehicleConfig::truckCenter;
    m_carSprite = VehicleBuildHelper::createCircleBody(scene, 20, spawnPosition, centerConfig.bodyOffset, centerConfig.getSpriteFilename(),
        centerConfig.physicsMaterial, categoryCarBody, colCarBody);
    m_body = m_carSprite->getPhysicsBody();
    m_carSprite->setScale(flipSign, 1);
    this->addChild(m_carSprite);

    //Engine
    auto engineLevel = VehicleConfig::engineLevels[vehicleSetup->getEngineLevel()];
    auto engineConfig = VehicleConfig::truckEngine;
    m_engine = Engine::create(scene, engineConfig, engineLevel, spawnPosition, categoryCarBody, colCarBody);
    VehicleBuildHelper::joinFixed(scene, m_body, m_engine->getPhysicsBody(), spawnPosition);
    m_engine->setScale(flipSign, 1);
    this->addChild(this->m_engine);

    //Wheels
    auto suspensionLevel = VehicleConfig::suspensionLevels[vehicleSetup->getSuspensionLevel()];
    auto wheelLevel = VehicleConfig::wheelLevels[vehicleSetup->getWheelLevel()];
    vector<WheelConfig> truckWheels = VehicleConfig::truckWheels;
    for (WheelConfig config : truckWheels)
    {
        Vec2 suspensionExtension = Vec2(0, -suspensionLevel.springLength);
        Vec2 wheelPos = spawnPosition + config.position + suspensionExtension;

        Wheel *wheel = Wheel::create(scene, wheelLevel.spriteFilePath, wheelLevel.radius, wheelLevel.braking, wheelPos,
            (flipX ? !config.isDriveWheel : config.isDriveWheel), wheelLevel.physicsMaterial, categoryCarBody, colCarBody);
        this->addChild(wheel);
        wheel->setLocalZOrder(50);

        Sprite *slotFrontSprite = VehicleBuildHelper::createCircleBody(scene, 10.0f, wheelPos, Vec2(0.0f, 0.0f), "brake_plate.png",
            PhysicsMaterial(0.15, 0.2, 0.95), noCol, noCol);
        PhysicsBody *slotFront = slotFrontSprite->getPhysicsBody();
        this->addChild(slotFrontSprite);

        //Suspension's groove joint. Moves up and down and attaches to a wheel pin
        auto frontGroove = PhysicsJointGroove::construct(m_body, wheel->getBody(), config.position, config.position + suspensionExtension,
            Vec2(0, 0));
        scene.getPhysicsWorld()->addJoint(frontGroove);

        //Pin is the point where wheel is attached to. Moves with spring / suspension
        auto frontPin = PhysicsJointPin::construct(slotFront, wheel->getBody(), wheelPos);
        scene.getPhysicsWorld()->addJoint(frontPin);

        //Suspension spring
        auto frontSpring = PhysicsJointSpring::construct(m_body, wheel->getBody(), config.position, Vec2(0, 0), suspensionLevel.springPower,
            suspensionLevel.springDamping);
        frontSpring->setRestLength(suspensionLevel.springLength);
        scene.getPhysicsWorld()->addJoint(frontSpring);

        m_wheels.push_back(move(wheel));
    }

    //Cosmetic parts
    for (VehiclePartConfig config : vehicleSetup->getCosmetics())
    {
        Vec2 pos = spawnPosition + Vec2(flipSign * config.position.x, config.position.y);
        Sprite *sprite = VehicleBuildHelper::createRectangleBody(scene, config.bodySize, pos, config.bodyOffset,
            config.getSpriteFilename(), config.physicsMaterial, categoryCarBody, colCarBody);
        this->addChild(sprite);

        VehicleBuildHelper::joinFixed(scene, m_body, sprite->getPhysicsBody(), spawnPosition);
        sprite->setScale(flipSign, 1);
        if (config.isDetachable)
            detachableParts.push_back(sprite);
    }

    //Exhaust pipe
    m_exhaustPipe = ExhaustPipe::create("exhaust_pipe.png", "smoke_exhaust.plist", "flame_exhaust.plist");
    m_carSprite->addChild(m_exhaustPipe);
    m_exhaustPipe->setPosition(Vec2(72, 65));

    //Status indicator
    m_glowSprite = VehicleBuildHelper::createSprite(Vec2(0, 0), "engine_lit.png", nullptr);
    m_glowSprite->setColor(Color3B::RED);
    m_glowSprite->setBlendFunc({GL_ONE, GL_ONE}); //Linear dodge / additive blend
    m_glowSprite->setLocalZOrder(100);
    this->addChild(m_glowSprite);

    //Brake light
    m_brakeLight = Sprite::create("brake_light.png");
    m_carSprite->addChild(m_brakeLight);
    m_brakeLight->setPosition(Vec2(152, 30));
    m_brakeLight->setLocalZOrder(100);

    //Inputs
    m_vehicleInput = VehicleInput::create();
    addChild(m_vehicleInput);
    m_vehicleInput->setVehicle(this);
    if (playerControlled)
        m_vehicleInput->listenPlayerInput();
    else
        m_vehicleInput->listenAIInput();

    //Rope attachment position
    m_anchorPosition = Vec2(13, 33);

    //wait until race has started before vehicles can move
    m_allowMovement = false;

    //Race start and end event handlers
    auto *raceStartListener = EventListenerCustom::create(Race::EVT_RACE_STARTED, CC_CALLBACK_1(Vehicle::raceStartHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceStartListener, this);

    auto *raceEndListener = EventListenerCustom::create(Race::EVT_RACE_ENDED, CC_CALLBACK_1(Vehicle::raceEndHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceEndListener, this);

    this->scheduleUpdate();

    return true;
}

void Vehicle::setPosition(Vec2 pos)
{
    if (m_carSprite)
        m_carSprite->setPosition(pos);
}

void Vehicle::update(float dt)
{
    //Shadow position below car
    m_shadow->setPosition(getPosition().x, 50);
    m_shadow->setOpacity(MathUtil::lerp((getPosition().y - 80) * 0.05, 50, 2));

    //Destroy regular cosmetics parts if engine is not completely destroyed
    if (!detachableParts.empty() && m_engine->getDamage() < 0.99f && m_engine->getDamage() - m_prevDropDamage > 0.25f)
    {
        int randomIndex = rand() % (detachableParts.size());
        auto body = detachableParts[randomIndex]->getPhysicsBody();
        RemoveJoints(body);
        detachableParts.erase(detachableParts.begin());
        m_prevDropDamage = m_engine->getDamage();
    }

    //Launch engine in the air and disable player controls when engine gets destroyed
    if (m_engine->getDamage() >= 1.0 && m_destroyed == false)
    {
        Vec2 launchDir = Vec2(0, 1) * (200 + rand() % 200);
        float angularVel = rand() % 30 - 15;
        PhysicsBody *engineBody = m_engine->getPhysicsBody();
        RemoveJoints(engineBody);
        engineBody->setVelocity(launchDir);
        engineBody->setAngularVelocity(angularVel);
        setGasLevel(0);
        setBrake(false);
        m_destroyed = true;
    }

    //Follow sprite position and glow redder depending on engine damage
    GLubyte redness = (GLubyte) (m_engine->getDamage() * 255);
    m_glowSprite->setColor(Color3B(redness, 0, 0));
    Vec2 pos = m_engine->convertToWorldSpace(m_engine->getSprite()->getPosition());
    m_glowSprite->setPosition(pos);
    m_glowSprite->setRotation(m_engine->getSprite()->getRotation());

    if (m_exhaustPipe)
        m_exhaustPipe->setTemperature(m_engine->getTemperature());

    EventCustom event((m_playerControlled ? Vehicle::EVT_PLAYER_VEHICLE_UPDATED : Vehicle::EVT_AI_VEHICLE_UPDATED));
    event.setUserData(this);
    _eventDispatcher->dispatchEvent(&event);
}

void Vehicle::setGasLevel(float gasLevel)
{
    float gas = gasLevel;
    if (m_destroyed || !m_allowMovement)
        gas = 0;

    m_exhaustPipe->setGasValue(gas);
    if (m_allowMovement)
    {
        int torqueDir = m_playerControlled ? -1 : 1;
        m_engine->setGasInput(gas, torqueDir, m_wheels);
    }
}

void Vehicle::setBrake(bool brake)
{
    //Brake forced on when game start is starting or when game has ended
    if (!m_allowMovement)
        brake = true;

    if (m_destroyed)
        return;

    m_brakeLight->setTexture(brake ? "brake_light_lit.png" : "brake_light.png");
    for (auto wheel : m_wheels)
        wheel->setBrake(brake);
}

void Vehicle::RemoveJoints(PhysicsBody *body)
{
    auto joints = body->getJoints();
    for (int i = 0; i < joints.size(); i++)
        joints[i]->removeFormWorld();
}

void Vehicle::raceStartHandler(EventCustom *event)
{
    m_allowMovement = true;
}

void Vehicle::raceEndHandler(EventCustom *event)
{
    m_allowMovement = false;
}
