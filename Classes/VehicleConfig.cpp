//
// Created by temiv on 4/10/2019.
//

#include "VehicleConfig.h"


//Level upgrade values
const vector<EngineLevel> VehicleConfig::engineLevels = {
        EngineLevel("engine_lvl1.png", 0, 30000000, 1.1f, Vec2(0, 0), PhysicsMaterial(1.3, 0.1, 0.75)),
        EngineLevel("engine_lvl2.png", 300, 37000000, 1.0f, Vec2(0, 0), PhysicsMaterial(1.7, 0.1, 0.75)),
        EngineLevel("engine_lvl3.png", 1000, 42000000, 0.9f, Vec2(0, 0), PhysicsMaterial(2.3, 0.1, 0.75))
};
const vector<WheelLevel> VehicleConfig::wheelLevels = {
        WheelLevel("wheel_lvl1.png", 0, 19, 1.0f, PhysicsMaterial(0.02, 0.05, 7.1)),
        WheelLevel("wheel_lvl2.png", 300, 22, 2.0f, PhysicsMaterial(0.017, 0.05, 9.5)),
        WheelLevel("wheel_lvl3.png", 800, 30, 3.0f, PhysicsMaterial(0.015, 0.05, 11.0))
};
const vector<SuspensionLevel> VehicleConfig::suspensionLevels = {
        SuspensionLevel("suspension_lvl1.png", 0, 15.0f, 32511.0f, 1400.0f),
        SuspensionLevel("suspension_lvl2.png", 250, 20.0f, 35000.0f, 1600.0f),
        SuspensionLevel("suspension_lvl3.png", 600, 35.0f, 39000.0f, 1800.0f)
};


PhysicsMaterial bodyMat = PhysicsMaterial(0.25, 0.2, 0.75);
PhysicsMaterial cosmeticsMat = PhysicsMaterial(0.02, 0.1, 0.85);

//Truck parts and each part types' variations
const vector<VehiclePartConfig> VehicleConfig::truckBackFrames = {
        VehiclePartConfig({"frame_back.png"}, Vec2(62, -10), Size(100, 10), Vec2(0.0f, -15), false, bodyMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckFrontFrames = {
        VehiclePartConfig({"frame_front.png"}, Vec2(-40, 0), Size(70, 7), Vec2(0.0f, -15), false, bodyMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckBuckets = {
        VehiclePartConfig(
                {
                        "bucket.png",
                        "bucket_red.png",
                        "bucket_black.png",
                        "bucket_purple.png",
                        "bucket_rust.png"
                },
                Vec2(66, -9), Size(80, 30), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "bucket_caged.png",
                        "bucket_caged_red.png",
                        "bucket_caged_black.png",
                        "bucket_caged_purple.png",
                        "bucket_caged_rust.png"
                },
                Vec2(66, 4), Size(80, 30), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckCabinFronts = {
        VehiclePartConfig(
                {
                        "cabin_front.png",
                        "cabin_front_red.png",
                        "cabin_front_black.png",
                        "cabin_front_purple.png",
                        "cabin_front_rust.png"
                },
                Vec2(-37, -7), Size(20, 40), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckCabins = {
        VehiclePartConfig(
                {
                        "cabin.png",
                        "cabin_red.png",
                        "cabin_black.png",
                        "cabin_purple.png",
                        "cabin_rust.png"
                },
                Vec2(-1, 10), Size(40, 50), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "cabin_hoodless.png",
                        "cabin_hoodless_red.png",
                        "cabin_hoodless_black.png",
                        "cabin_hoodless_purple.png",
                        "cabin_hoodless_rust.png"
                },
                Vec2(25, -7), Size(40, 50), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckSteppers = {
        VehiclePartConfig({"stepper.png"}, Vec2(-1, -31), Size(50, 3), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckBackFenders = {
        VehiclePartConfig(
                {
                        "fender_back.png",
                        "fender_back_red.png",
                        "fender_back_black.png",
                        "fender_back_purple.png",
                        "fender_back_rust.png"
                },
                Vec2(75, -15), Size(70, 40), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "fender_back_square.png",
                        "fender_back_square_red.png",
                        "fender_back_square_black.png",
                        "fender_back_square_purple.png",
                        "fender_back_square_rust.png"
                },
                Vec2(65, -20), Size(70, 40), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckHoods = {
        VehiclePartConfig(
                {
                        "hood.png",
                        "hood_red.png",
                        "hood_black.png",
                        "hood_purple.png",
                        "hood_rust.png"
                },
                Vec2(-57, 7), Size(50, 20), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "hood_holed.png",
                        "hood_holed_red.png",
                        "hood_holed_black.png",
                        "hood_holed_purple.png",
                        "hood_holed_rust.png"
                },
                Vec2(-57, 7), Size(50, 20), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckFrontFenders = {
        VehiclePartConfig(
                {
                        "fender_front.png",
                        "fender_front_red.png",
                        "fender_front_black.png",
                        "fender_front_purple.png",
                        "fender_front_rust.png"
                },
                Vec2(-59, -18), Size(55, 30), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "fender_back_square.png",
                        "fender_back_square_red.png",
                        "fender_back_square_black.png",
                        "fender_back_square_purple.png",
                        "fender_back_square_rust.png"
                },
                Vec2(-59, -15), Size(55, 30), Vec2(0, 0), true, cosmeticsMat)
};

const vector<VehiclePartConfig> VehicleConfig::truckDoors = {
        VehiclePartConfig(
                {
                        "door.png",
                        "door_red.png",
                        "door_black.png",
                        "door_purple.png",
                        "door_rust.png"
                },
                Vec2(-3, 9), Size(30, 40), Vec2(0, 0), true, cosmeticsMat),
        VehiclePartConfig(
                {
                        "door_hoodless.png",
                        "door_hoodless_red.png",
                        "door_hoodless_black.png",
                        "door_hoodless_purple.png",
                        "door_hoodless_rust.png"
                },
                Vec2(-3, -6), Size(30, 40), Vec2(0, 0), true, cosmeticsMat)
};


//Truck configs
const EngineConfig VehicleConfig::truckEngine = EngineConfig(Vec2(-50, -5), Size(40, 30));

const VehiclePartConfig VehicleConfig::truckCenter = VehiclePartConfig({"frame_center.png"}, Vec2(0.0f, 0.0f),
        Size(2, 2), Vec2(0, 0), false, bodyMat);

const vector<WheelConfig> VehicleConfig::truckWheels = {
        WheelConfig(Vec2(-63, -17), true),
        WheelConfig(Vec2(63, -17), false)
};
