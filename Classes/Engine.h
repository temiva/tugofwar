//
// Created by temiv on 4/15/2019.
//

#ifndef PROJ_ANDROID_ENGINE_H
#define PROJ_ANDROID_ENGINE_H

#include "cocos2d.h"
#include "VehicleConfig.h"
#include "Wheel.h"

using namespace cocos2d;
using namespace std;

class Engine : public Node
{
private:
    Sprite *m_sprite;
    float m_maxTorque;
    float m_overheatSpeed;
    float m_temperature = 0;
    float m_damage = 0;
    float m_gasLevel = 0;

public:
    static Engine *create(Scene &scene, EngineConfig &engineConfig, EngineLevel &engineLevel, Vec2 spawnPosition, int categoryBitmask,
        int collisionBitmask);

    bool
    init(Scene &scene, EngineConfig &engineConfig, EngineLevel &engineLevel, Vec2 spawnPosition, int categoryBitmask, int collisionBitmask);

    void setGasInput(float gasInput, int rotationDirection, vector<Wheel *> &wheels);

    void update(float delta) override;

    PhysicsBody *getPhysicsBody() { return m_sprite->getPhysicsBody(); };

    float getDamage() { return m_damage; };

    float getTemperature() { return m_temperature; };

    Sprite *getSprite() { return m_sprite; };
};


#endif //PROJ_ANDROID_ENGINE_H
