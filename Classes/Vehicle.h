//
// Created by temiv on 4/3/2019.
//

#ifndef PROJ_ANDROID_VEHICLE_H
#define PROJ_ANDROID_VEHICLE_H

#include <string>
#include <vector>
#include "cocos2d.h"
#include "VehicleInput.h"
#include "Wheel.h"
#include "ExhaustPipe.h"
#include "VehicleConfig.h"
#include "Engine.h"

using namespace cocos2d;
using namespace std;

/**
 * @brief Description of vehicle part levels and its cosmetic parts.
 */
class VehicleSetup
{
private:
    int m_engineLevel = 0;
    int m_wheelLevel = 0;
    int m_suspensionLevel = 0;
    vector<VehiclePartConfig> m_cosmetics;

public:
    VehicleSetup() : VehicleSetup(0, 0, 0)
    {};

    VehicleSetup(int engine, int wheel, int suspension)
    {
        m_engineLevel = max(min(engine, 2), 0);
        m_wheelLevel = max(min(wheel, 2), 0);
        m_suspensionLevel = max(min(suspension, 2), 0);
    };

    void setCosmetics(vector<VehiclePartConfig> cormeticsSetup)
    {
        m_cosmetics = cormeticsSetup;
    }

    vector<VehiclePartConfig> getCosmetics() { return m_cosmetics; };

    int getEngineLevel() { return m_engineLevel; };

    int getWheelLevel() { return m_wheelLevel; };

    int getSuspensionLevel() { return m_suspensionLevel; };

    int getReward() { return (m_engineLevel + 1) * 90 + (m_wheelLevel + 1) * 70 + (m_suspensionLevel + 1) * 60; };
};


/**
 * @brief Vehicle constructed from various vehicle parts that are attached together using physics joints.
 */
class Vehicle : public Node
{
private:
    Sprite *m_carSprite = nullptr;
    VehicleInput *m_vehicleInput = nullptr;
    PhysicsBody *m_body = nullptr;
    Sprite *m_shadow;
    Sprite *m_brakeLight;
    ExhaustPipe *m_exhaustPipe;
    vector<Wheel *> m_wheels;
    Engine *m_engine;
    Sprite *m_glowSprite;
    vector<Sprite *> detachableParts;

    Vec2 m_anchorPosition;
    bool m_playerControlled;
    float m_prevDropDamage = 0;
    bool m_destroyed = false;
    bool m_allowMovement = false;

    void RemoveJoints(PhysicsBody *body);

    //Event handlers
    void raceStartHandler(EventCustom *event);

    void raceEndHandler(EventCustom *event);

public:
    static const string EVT_PLAYER_VEHICLE_UPDATED;
    static const string EVT_AI_VEHICLE_UPDATED;

    static Vehicle *create(VehicleSetup *vehicleSetup, Scene &scene, Vec2 spawnPosition, bool playerControlled);

    bool init(VehicleSetup *vehicleSetup, Scene &scene, Vec2 spawnPosition, bool playerControlled);

    void update(float delta) override;

    void setPosition(Vec2 pos);

    Vec2 getPosition() { return m_carSprite->getPosition(); };

    Node *getRootNode() { return m_carSprite; };

    Vec2 getAnchorPosition() { return m_anchorPosition; };

    void setGasLevel(float gasLevel);

    void setBrake(bool brake);

    PhysicsBody *getBody() { return m_body; };

    Engine *getEngine() { return m_engine; };
};


#endif //PROJ_ANDROID_VEHICLE_H
