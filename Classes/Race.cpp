//
// Created by temiv on 4/8/2019.
//

#include "Race.h"

const float Race::DISTANCE_THRESSHOLD = 500;

const string Race::EVT_RACE_COUNTDOWN_SET = "EVT_RACE_COUNTDOWN_SET";
const string Race::EVT_RACE_STARTED = "EVT_RACE_STARTED";
const string Race::EVT_RACE_TIME_SET = "EVT_RACE_TIME_SET";
const string Race::EVT_RACE_ENDED = "EVT_RACE_ENDED";
const string Race::EVT_RACE_WON = "EVT_RACE_WON";
const string Race::EVT_RACE_LOST = "EVT_RACE_LOST";

void Race::startCountdown(Vehicle *player, Vehicle *enemy)
{
    m_player = player;
    m_enemy = enemy;
    m_raceTimer = 0;
    this->scheduleUpdate();
    m_raceStarted = false;
}

void Race::end()
{
    Vehicle *winner;
    if (m_player->getPosition().x > 0)
    {
        winner = m_player;
        EventCustom event(EVT_RACE_WON);
        _eventDispatcher->dispatchEvent(&event);
    }
    else
    {
        winner = m_enemy;
        EventCustom event(EVT_RACE_LOST);
        _eventDispatcher->dispatchEvent(&event);
    }

    EventCustom event(EVT_RACE_ENDED);
    event.setUserData(&winner);
    _eventDispatcher->dispatchEvent(&event);

    this->unscheduleUpdate();
}

void Race::update(float deltaTime)
{
    const int countDownTime = 4;
    const int raceLength = 30;
    int prevTime = (int) m_raceTimer;
    m_raceTimer += deltaTime;
    int curTime = (int) m_raceTimer;

    //Counting down
    if (curTime < countDownTime && prevTime != curTime)
    {
        int countdown = countDownTime - curTime;
        EventCustom event(EVT_RACE_COUNTDOWN_SET);
        event.setUserData(&countdown);
        _eventDispatcher->dispatchEvent(&event);
    }
        //Race should start
    else if (m_raceStarted == false && curTime >= countDownTime)
    {
        EventCustom event(EVT_RACE_STARTED);
        _eventDispatcher->dispatchEvent(&event);
        m_raceStarted = true;
    }
        //Race started but has not ended
    else if (m_raceStarted && curTime - countDownTime < raceLength)
    {
        if (prevTime != curTime)
        {
            int timer = raceLength - (curTime - countDownTime);
            EventCustom event(EVT_RACE_TIME_SET);
            event.setUserData(&timer);
            _eventDispatcher->dispatchEvent(&event);
        }
    }
        //Race timeout -> race has ended
    else if (m_raceStarted && curTime - countDownTime >= raceLength)
    {
        end();
    }

    //distance based win or lose condition
    if (m_player->getPosition().x > DISTANCE_THRESSHOLD || m_player->getPosition().x < -DISTANCE_THRESSHOLD)
        end();
}