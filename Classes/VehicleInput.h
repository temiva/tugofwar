//
// Created by temiv on 4/3/2019.
//

#ifndef PROJ_ANDROID_VEHICLEINPUT_H
#define PROJ_ANDROID_VEHICLEINPUT_H

#include "cocos2d.h"

class Vehicle;

using namespace std;
using namespace cocos2d;

struct VehicleInputValue
{
    float gasLevel;
    bool brake;
};

class VehicleInput : public Node
{
private:
    Vehicle *m_vehicle;

    void inputHandler(EventCustom *event);

public:
    static const string EVT_USER_INPUT_SET;
    static const string EVT_AI_INPUT_SET;

    CREATE_FUNC(VehicleInput);

    /**
    @brief  Starts to apply inputs to vehicle using player input events. Basically reacts to EVT_USER_INPUT_SET event
    */
    void listenPlayerInput();

    /**
    @brief  Starts to apply inputs to vehicle using AI input events. Basically reacts to EVT_AI_INPUT_SET event
    */
    void listenAIInput();

    /**
    @brief  Sets vehicle that is going to receive the inputs
    @param vehicle that will receive inputs
    */
    void setVehicle(Vehicle *vehicle) { m_vehicle = vehicle; };
};


#endif //PROJ_ANDROID_VEHICLEINPUT_H
