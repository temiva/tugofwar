//
// Created by temiv on 4/13/2019.
//

#ifndef PROJ_ANDROID_UPGRADEFRAME_H
#define PROJ_ANDROID_UPGRADEFRAME_H

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include <string>

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace std;


/**
 * @brief UI element for updating vehicle parts.
 */
class UpgradeFrame : public Node
{
private:
    Button *m_upgradeButton;
    Button *m_downgradeButton;
    Sprite *m_previewSprite;
    Label *m_priceLabel;
    function<void()> m_downgradeFunction;
    function<void()> m_upgradeFunction;

    void onUpgradeButton(Ref *pSender, Widget::TouchEventType type);

    void onDowngradeButton(Ref *pSender, Widget::TouchEventType type);

public:
    static UpgradeFrame *create(const string &previewSprite, function<void()> upgradeFunction, function<void()> downgradeFunction);

    bool init(const string &previewSpriteFile, function<void()> upgradeFunction, function<void()> downgradeFunction);

    void setSpriteTexture(const string &spriteFilename);

    void setEnableUpgrade(bool enable);

    void setEnableDowngrade(bool enable);

    void setPrice(int price);

    void setShowPrice(bool show);
};


#endif //PROJ_ANDROID_UPGRADEFRAME_H
