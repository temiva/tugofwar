//
// Created by temiv on 4/9/2019.
//

#include "Gauge.h"
#include <tgmath.h>
#include <random>


bool Gauge::init()
{
    if (!Node::init())
        return false;

    auto backgroundSprite = Sprite::create("gauge_temp_background.png");
    this->addChild(backgroundSprite);

    m_warningBackground = Sprite::create("gauge_temp_background_warning.png");
    this->addChild(m_warningBackground);
    m_warningBackground->setPosition(Vec2(0, 0));

    m_pin = Sprite::create("gauge.png");
    m_pin->setAnchorPoint(Vec2(0.5f, 0.25f));
    this->addChild(m_pin);
    this->scheduleUpdate();

    return true;
}


void Gauge::setValue(float val)
{
    float rndShake = 0;
    if (val > 0.6f)
        rndShake = (rand() % 1000) * 0.01f * val;
    m_val = val;
    m_pin->setRotation(m_val * 180 - 90 + rndShake);
}


void Gauge::update(float dt)
{
    m_flashTimer += dt;
    if (m_warningBackground && m_val > 0.8f)
    {
        float sinState = (sin(m_flashTimer * 10.0f) + 1.0f) * 0.5f;

        m_warningBackground->setOpacity(sinState * 255);
    }
    else
        m_warningBackground->setOpacity(0);
}
