//
// Created by temiv on 4/2/2019.
//

#include "RaceScene.h"
#include "SimpleAudioEngine.h"
#include "VehicleInput.h"
#include "VehicleBuildHelper.h"

RaceScene *RaceScene::create(SceneLoader *gameState, Session *sessionData)
{
    RaceScene *scene = new(std::nothrow) RaceScene;
    if (scene && scene->init(gameState, sessionData))
    {
        scene->autorelease();
        return scene;
    }

    delete scene;
    scene = nullptr;
    return nullptr;
}

bool RaceScene::init(SceneLoader *gameState, Session *sessionData)
{
    if (!Scene::initWithPhysics())
    {
        return false;
    }
    m_gameState = gameState;
    m_sessionData = sessionData;

    getPhysicsWorld()->setGravity(Vec2(0, -131));
    //getPhysicsWorld()->setDebugDrawMask(0xffff);

    //Allow more substeps for physics accuracy.
    getPhysicsWorld()->setSubsteps(10);

    //120FPS for physics for more stable physics. Pretty excessive for a mobile game though :P
    getPhysicsWorld()->setFixedUpdateRate(120);


    setupSceneSprites();
    setupVehicles();
    setupCameras();

    m_race = Race::create();
    addChild(m_race);
    m_race->startCountdown(m_vehiclePlayer, m_vehicleAI);

    //User touch layer
    auto touchLayer = RaceUILayer::create(m_gameState);
    touchLayer->setCameraMask((unsigned short) CameraFlag::USER1, true);
    touchLayer->setLocalZOrder(1000);
    addChild(touchLayer);

    //Race won listener
    EventListenerCustom *raceWonListener = EventListenerCustom::create(Race::EVT_RACE_WON, CC_CALLBACK_1(RaceScene::raceWonHandler, this));
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(raceWonListener, this);

    this->scheduleUpdate();

    return true;
}

void RaceScene::createGround()
{
    auto groundBody = PhysicsBody::createBox(Size(100000, 30));
    groundBody->setDynamic(false);
    //Any other way to create invisible physics body???
    auto groundSprite = Sprite::create("transparent.png");
    groundSprite->addComponent(groundBody);
    addChild(groundSprite);
    groundSprite->setPosition((Vec2(0, 30)));
}

void
RaceScene::ConnectVehicles(PhysicsBody *bodyA, PhysicsBody *bodyB, Vec2 localAttachPointA, Vec2 localAttachPointB, float connectionDistance)
{
    auto connectionJoint = PhysicsJointLimit::construct(bodyA, bodyB, localAttachPointA, localAttachPointB, 0.0f, connectionDistance);
    getPhysicsWorld()->addJoint(connectionJoint);

    m_ropeSprite = Sprite::create("rope.png");
    this->addChild(m_ropeSprite);
    m_ropeSprite->setLocalZOrder(-1);
}

void RaceScene::setupCameras()
{
    auto windowsSize = Director::getInstance()->getWinSize();

    removeChild(getDefaultCamera());

    //World object perspective camera
    m_worldCamera = Camera::createPerspective(45, (GLfloat) windowsSize.width / windowsSize.height, 100, 10000);
    m_worldCamera->lookAt(Vec3(0, 0, 0), Vec3(0, 0, 0));
    m_worldCamera->setPositionZ(400);
    m_worldCamera->setPosition(0, 0);
    addChild(m_worldCamera);

    //HUD camera
    m_camHUD = Camera::create();
    m_camHUD->setCameraFlag(CameraFlag::USER1);
    addChild(m_camHUD);
}

void RaceScene::setupSceneSprites()
{
    Sprite *background = Sprite::create("background.png");
    background->setPosition(0, 0);
    background->setAnchorPoint(Vec2(0.5f, 0.3f));
    background->setScale(11.4f);
    background->setLocalZOrder(-100);
    background->setPositionZ(-9000);
    addChild(background);

    Sprite *midground = Sprite::create("midground.png");
    midground->setPosition(0, 0);
    midground->setAnchorPoint(Vec2(0.5f, 0.46f));
    midground->setLocalZOrder(-1);
    midground->setScale(1.3f, 1.1f);
    addChild(midground);

    Sprite *foreground = Sprite::create("foreground.png");
    foreground->setPosition(0, 0);
    foreground->setAnchorPoint(Vec2(0.5f, 0.0f));
    foreground->setScale(0.3f);
    addChild(foreground);
    foreground->setLocalZOrder(100);
    foreground->setPositionZ(300);

    createGround();
}

void RaceScene::setupVehicles()
{
    //Player vehicle
    VehicleSetup playerLevels = VehicleSetup(m_sessionData->getEngineLevel(), m_sessionData->getWheelLevel(),
        m_sessionData->getSuspensionLevel());
    playerLevels.setCosmetics(Session::vehicleCosmetics); //Always the same default cosmetic parts for player
    m_vehiclePlayer = Vehicle::create(&playerLevels, *this, Vec2(0, 150), true);

    //AI vehicle
    VehicleSetup *aiLevels = m_sessionData->getEnemyVehicleSetup();
    m_vehicleAI = Vehicle::create(aiLevels, *this, Vec2(-250, 150), false);
    m_aiController = AIController::create(m_vehicleAI);
    addChild(m_aiController);

    //Rope connecting the vehicles
    float ropeDst = m_vehicleAI->getPosition().distance(m_vehiclePlayer->getPosition()) + 50;
    ConnectVehicles(m_vehiclePlayer->getBody(), m_vehicleAI->getBody(), m_vehiclePlayer->getAnchorPosition(),
        m_vehicleAI->getAnchorPosition(), ropeDst);
}

void RaceScene::raceWonHandler(EventCustom *event)
{
    //Add victory money to player
    m_sessionData->addMoney(m_sessionData->getEnemyVehicleSetup()->getReward());
}

void RaceScene::update(float dt)
{
    if (m_vehiclePlayer && m_vehicleAI)
    {
        Vec2 rootSpaceStart = Vec2(m_vehiclePlayer->getRootNode()->getContentSize().width * 0.5f,
            m_vehiclePlayer->getRootNode()->getContentSize().height * 0.5f) + m_vehiclePlayer->getAnchorPosition();
        Vec2 ropeStart = m_vehiclePlayer->getRootNode()->convertToWorldSpace(rootSpaceStart);
        Vec2 rootSpaceEnd =
            Vec2(m_vehicleAI->getRootNode()->getContentSize().width * 0.5f, m_vehicleAI->getRootNode()->getContentSize().height * 0.5f) +
            m_vehicleAI->getAnchorPosition();
        Vec2 ropeEnd = m_vehicleAI->getRootNode()->convertToWorldSpace(rootSpaceEnd);
        Vec2 dir = ropeEnd - ropeStart;
        float angleRads = dir.getAngle();

        m_ropeSprite->setRotation(CC_RADIANS_TO_DEGREES(-angleRads));
        m_ropeSprite->setPosition(ropeEnd);
        m_ropeSprite->setAnchorPoint(Vec2(1.0, 0.5f));
        float stretch = dir.getLength() / m_ropeSprite->getContentSize().width;
        m_ropeSprite->setScale(stretch, 1.0f);
    }

    //Move camera to center of action
    m_worldCamera->setPosition((m_vehicleAI->getPosition() + m_vehiclePlayer->getPosition()) * 0.5f);
    float dst = m_vehicleAI->getPosition().distance(m_vehiclePlayer->getPosition()) + 200;
    m_worldCamera->setPositionZ(dst);
}
