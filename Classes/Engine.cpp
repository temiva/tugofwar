//
// Created by temiv on 4/15/2019.
//

#include "Engine.h"
#include "VehicleBuildHelper.h"


Engine *Engine::create(Scene &scene, EngineConfig &engineConfig, EngineLevel &engineLevel, Vec2 spawnPosition, int categoryBitmask,
    int collisionBitmask)
{
    Engine *engine = new(std::nothrow) Engine;
    if (engine && engine->init(scene, engineConfig, engineLevel, spawnPosition, categoryBitmask, collisionBitmask))
    {
        engine->autorelease();
        return engine;
    }
    delete engine;
    engine = nullptr;
    return nullptr;
}

bool Engine::init(Scene &scene, EngineConfig &engineConfig, EngineLevel &engineLevel, Vec2 spawnPosition, int categoryBitmask,
    int collisionBitmask)
{
    if (!Node::init())
        return false;

    m_maxTorque = engineLevel.torque;
    m_overheatSpeed = engineLevel.overheatSpeed;

    m_sprite = VehicleBuildHelper::createRectangleBody(scene, engineConfig.bodySize, spawnPosition + engineConfig.position,
        engineLevel.positionOffset, engineLevel.spriteFilePath, engineLevel.physicsMaterial, categoryBitmask, collisionBitmask);

    this->addChild(m_sprite);
    this->scheduleUpdate();

    return true;
}

void Engine::update(float dt)
{
    //Cooldownd speed is faster at low temps and slower at high temps. Its more risky to keep it at high temps
    const float cooldownSpeed = 0.1f + (1.0f - (m_temperature * m_temperature)) * 0.3f;
    if (m_gasLevel > 0.1f)
        m_temperature += dt * m_gasLevel * 0.25f * m_overheatSpeed;
    else
        m_temperature -= dt * cooldownSpeed * (1.0 / m_overheatSpeed);

    //clamp to 0-1
    m_temperature = min(1.0f, max(m_temperature, 0.0f));

    //Increase engine damage if temperature is maxed
    if (m_temperature > 0.95f)
    {
        m_damage += dt * 0.4f;
    }
}

void Engine::setGasInput(float gasInput, int rotationDirection, vector<Wheel *> &wheels)
{
    m_gasLevel = gasInput;
    for (auto wheel : wheels)
    {
        wheel->setTorque(m_gasLevel * (m_maxTorque + MathUtil::lerp(0, m_maxTorque, m_temperature)) * rotationDirection);
    }
}
